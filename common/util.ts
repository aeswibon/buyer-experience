export function capitalizeFirst(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export function numberToDigitString(numbr: number) {
  return `${numbr > 9 ? numbr : `0${numbr}`}`;
}

export function convertToCaseSensitiveLocale(localeCode: string) {
  const parts = localeCode.split('-');

  if (parts.length === 2) {
    return `${parts[0].toLowerCase()}-${parts[1].toUpperCase()}`;
  }

  return localeCode.toLowerCase();
}

export function getUrlFromContentfulImage(contentfulImage: any) {
  return contentfulImage?.fields?.file?.url;
}
