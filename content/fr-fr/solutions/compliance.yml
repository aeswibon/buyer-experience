---
  title: GitLab, l'assurance de la conformité continue des logiciels
  description: Comment utiliser GitLab pour créer des applications conformes avec une chaîne d'approvisionnement logicielle sécurisée.
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          -  Automatisez la conformité, réduisez les risques
        title: Garantie de conformité des logiciels grâce à GitLab
        subtitle: Créez des applications qui répondent aux normes réglementaires communes grâce à une chaîne d'approvisionnement logicielle sécurisée.
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Essayer Ultimate gratuitement
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: En savoir plus sur la tarification
          url: /pricing/
          data_ga_name: pricing
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/compliance/compliance-hero.jpeg
          image_url_mobile: /nuxt-images/solutions/compliance/compliance-hero.jpeg
          alt: "Image:GitLab pour le secteur public"
          bordered: true
    - name: 'by-industry-intro'
      data:
        intro_text: Adoptée par
        logos:
          - name: Logo Duncan Aviation
            image: "/nuxt-images/logos/duncan-aviation-logo.svg"
            url: /customers/duncan-aviation/
            aria_label: Lien vers l'étude de cas Duncan Aviation
          - name: Logo Curve
            image: /nuxt-images/logos/curve-logo.svg
            url: /customers/curve/
            aria_label: Lien vers l'étude de cas Curve
          - name: Logo Hilti
            image: /nuxt-images/logos/hilti_logo.svg
            url: /customers/hilti/
            aria_label: Lien vers l'étude de cas Hilti
          - name: Logo The Zebra
            image: /nuxt-images/logos/zebra.svg
            url: /customers/thezebra/
            aria_label: Lien vers l'étude de cas The Zebra
          - name: Logo New 10
            image: /nuxt-images/logos/new10-logo.svg
            url: /customers/new10/
            aria_label: Lien vers l'étude de cas Conversica
          - name: Logo Chorus
            image: /nuxt-images/logos/chorus-logo.svg
            url: /customers/chorus/
            aria_label: Lien vers l'étude de cas Bendigo and Adelaide Bank
    - name: 'side-navigation-variant'
      links:
        - title: Présentation
          href: '#overview'
        - title: Capacités
          href: '#capabilities'
        - title: Clients
          href: '#customers'
        - title: Tarifs
          href: '#pricing'
        - title: Ressources
          href: '#resources'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Simplifiez et automatisez la conformité logicielle
                image:
                  image_url: "/nuxt-images/solutions/compliance/compliance-overview.png"
                  alt: Conformité continue de la livraison de logiciels avec GitLab
                is_accordion: false
                items:
                  - icon:
                      name: devsecops
                      alt: Icône DevSecOps
                      variant: marketing
                    header: Gestion des risques
                    text: Faites plus que simplement minimiser les failles de sécurité dans le code lui-même
                  - icon:
                      name: clipboard-check
                      alt: Icône de presse-papiers avec une coche
                      variant: marketing
                    header: Simple et sans friction
                    text: Une expérience intégrée pour définir, appliquer et rendre compte de la conformité
                  - icon:
                      name: release
                      alt: Icône de bouclier avec une coche
                      variant: marketing
                    header: Mise en place de garde-fous
                    text: Contrôlez l'accès et mettez en place des politiques
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                markdown: true
                subtitle: Rapide, sécurisé, conforme.
                sub_description: ''
                white_bg: true
                sub_image: /nuxt-images/solutions/compliance/compliance-benefits.png
                solutions:
                  - title: Gestion des politiques
                    description: Définissez des règles et des politiques pour respecter les cadriciels de conformité et les contrôles communs
                    list:
                      - "**Rôles et autorisations des utilisateurs granulaires :** définissez les rôles des utilisateurs et les niveaux d'autorisation qui conviennent à votre entreprise"
                      - "**Contrôle d'accès :** limitez l'accès avec des jetons d'authentification et d'expiration à deux facteurs"
                      - "**Paramètres de conformité:** définissez et appliquez des politiques de conformité pour des projets, des groupes et des utilisateurs spécifiques"
                      - "**Inventaire des identifiants :** gardez une trace de tous les identifiants qui peuvent être utilisés pour accéder à une instance autogérée de GitLab"
                      - "**Branches protégées :** contrôlez les modifications non autorisées de branches spécifiques (y compris la création, le transfert et la suppression d'une branche) sans autorisations ou approbations adéquates"
                    link_text: En savoir plus
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html?_gl=1*1r05yn6*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2NTY1NDE3OC4xNDguMS4xNjY1NjU1ODM3LjAuMC4w#policy-management
                    data_ga_name: policy mnagement
                    data_ga_location: solutions block
                  - title: Automatisation du flux de travail conforme
                    description: Faites respecter les règles définies, les politiques et la séparation des tâches tout en réduisant le risque global pour l'entreprise
                    list:
                      - "**Modèles de projets du cadriciel de conformité :** créez des projets qui correspondent à des protocoles d'audit spécifiques tels que HIPAA pour aider à maintenir une piste d'audit et à gérer les programmes de conformité"
                      - "**Label de projet du cadriciel de conformité :** appliquez facilement des paramètres de conformité communs à un projet à l'aide d'un label"
                      - "**Pipelines du cadriciel de conformité :** définissez les tâches de conformité qui doivent être exécutées dans chaque pipeline pour garantir l'exécution des analyses de sécurité, la création et le stockage des artefacts ou toute autre étape requise par vos exigences organisationnelles"
                    link_text: En savoir plus
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html?_gl=1*nbfxzt*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2NTY1NDE3OC4xNDguMS4xNjY1NjU2NDIyLjAuMC4w#compliant-workflow-automation
                    data_ga_name: workflow automation
                    data_ga_location: solutions block
                  - title: Gestion des audits
                    description: Préparez-vous aux audits et comprenez mieux les causes profondes des problèmes grâce à un accès facile aux données d'audit
                    list:
                      - "**[Événements d'audit :](https://docs.gitlab.com/ee/administration/audit_events.html)** suivez les événements importants tels que les modifications des niveaux d'autorisation des utilisateurs, l'ajout d'un nouvel utilisateur ou la suppression d'un utilisateur"
                      - "**[Événements d'audit en streaming:](https://docs.gitlab.com/ee/administration/audit_event_streaming.html)** consolidez vos journaux d'audit dans un outil de votre choix"
                      - "**[Rapports d'audit :](https://docs.gitlab.com/ee/administration/audit_reports.html)** répondez aux auditeurs en générant des rapports complets tels que les événements relatifs aux instances, aux groupes et aux projets, les données d'empreint d'identité, la connexion et les événements relatifs à l'utilisateur"
                      - "**[Rapport de conformité :](https://docs.gitlab.com/ee/user/compliance/compliance_report/)** obtenez une vue d'ensemble des violations de la conformité et des raisons et de la gravité des violations dans les requêtes de fusion"
                  - title: Gestion des vulnérabilités et des dépendances
                    description: Affichez, triez, suivez les tendances et résolvez les vulnérabilités et les dépendances dans vos applications
                    list:
                      - "**[Tableaux de bord de sécurité :](https://docs.gitlab.com/ee/user/application_security/security_dashboard/)** accédez aux applications de statut de sécurité actuelles et lancez la correction"
                      - "**[Nomenclature logicielle:](https://docs.gitlab.com/ee/user/application_security/dependency_list/)** recherchez les failles de sécurité dans les dépendances des applications et des conteneurs et créez une nomenclature logicielle (SBOM) des dépendances utilisées"
        - name: 'div'
          id: 'customers'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                align: left
                header: |
                  Approuvée par les entreprises.
                  <br />
                  Adorée par les développeurs.
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/duncan-aviation-logo.svg
                      alt: Logo Duncan Aviation
                    quote: GitLab nous a aidés à automatiser les processus manuels à l'aide de pipelines. Aujourd'hui, nous déployons régulièrement du code, ce qui nous permet d'apporter à nos clients des modifications et des correctifs essentiels beaucoup plus rapidement.
                    author: Ben Ferguson
                    position: Senior Programmer, Duncan Aviation
                    ga_carousel: duncan aviation testimonial
                    url: /customers/duncan-aviation/
                  - title_img:
                      url: /nuxt-images/logos/curve-logo.svg
                      alt: Logo Curve
                    quote: Avant d'adopter GitLab, la charge de travail des équipes opérationnelles était considérable. Il était très difficile de donner aux développeurs les moyens de faire leur travail de manière efficace. Le choix le plus évident était de tout regrouper en un seul endroit et que tout soit disponible à travers un seul panneau 
                    author: Ed Shelto
                    position: Site Reliability Engineer, Curve
                    ga_carousel: curve testimonial
                    url: /customers/curve/
                  - title_img:
                      url: /nuxt-images/logos/hilti_logo.svg
                      alt: Logo Hilti
                    quote: La solution GitLab est empaquetés comme une suite et livrés avec un installateur très sophistiqué. Et pourtant tout fonctionne. C'est très pratique si vous êtes une entreprise qui souhaite simplement le mettre en place et le rendre opérationnel.
                    author: Daniel Widerin
                    position: Head of Software Delivery, HILTI
                    ga_carousel: hilti testimonial
                    url: /customers/hilti/
        - name: div
          id: 'pricing'
          slot_enabled: true
          slot_content:
            - name: 'tier-block'
              class: 'slp-mt-96'
              id: 'pricing'
              data:
                header: Quelle édition vous convient le mieux ?
                cta:
                  url: /pricing/
                  text: En savoir plus sur la tarification
                  data_ga_name: pricing
                  data_ga_location: free tier
                  aria_label: pricing
                tiers:
                  - id: free
                    title: Forfait Gratuit
                    items:
                      - Tests statiques de sécurité des applications (SAST) et détection des secrets
                      - Résultats dans le fichier json
                    link:
                      href: /pricing/
                      text: En savoir plus
                      data_ga_name: pricing
                      data_ga_location: free tier
                      aria_label: free tier
                  - id: premium
                    title: Premium
                    items:
                      - Tests statiques de sécurité des applications (SAST) et détection des secrets
                      - Résultats dans le fichier json
                      - Approbations MR et contrôles plus courants
                    link:
                      href: /pricing/
                      text: En savoir plus
                      data_ga_name: pricing
                      data_ga_location: premium tier
                      aria_label: premium tier
                  - id: ultimate
                    title: Ultimate
                    items:
                      - Tout ce qui compose l'édition Premium, plus
                      - Les scanners de sécurité complets incluent SAST, DAST, Secrets, dépendances, conteneurs, IaC, API, images de clusters et tests à données aléatoires
                      - Résultats exploitables dans le pipeline MR
                      - Pipelines de conformité
                      - Tableaux de bord de sécurité et de conformité
                      - Et bien plus encore
                    link:
                      href: /pricing/
                      text: En savoir plus
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: ultimate tier
                    cta:
                      href: /free-trial/
                      text: Essayer Ultimate gratuitement
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: ultimate tier
        - name: 'div'
          id: 'resources'
          slot_enabled: true
          slot_content:
          - name: solutions-resource-cards
            data:
              title: Ressources connexes
              align: left
              column_size: 4
              grouped: true
              cards:
                - icon:
                    name: video
                    variant: marketing
                    alt: Icône de vidéo
                  event_type: "Vidéo"
                  header: Pipelines conformes
                  link_text: "Regarder maintenant"
                  image: "/nuxt-images/solutions/compliance/compliant-pipelines.jpeg"
                  href: https://www.youtube.com/embed/jKA_e_jimoI
                  data_ga_name: Compliant pipelines
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Icône de vidéo
                  event_type: "Vidéo"
                  header: Conformité continue du logiciel
                  link_text: "Regarder maintenant"
                  image: "/nuxt-images/solutions/compliance/continuous-software-compliance.jpeg"
                  href: https://player.vimeo.com/video/694891993?h=7768f52e29
                  data_ga_name: Conformité continue du logiciel
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Icône de vidéo
                  event_type: "Vidéo"
                  header: SBOM et attestation
                  link_text: "Regarder maintenant"
                  image: "/nuxt-images/solutions/compliance/sbom-and-attestation.jpeg"
                  href: https://www.youtube.com/embed/wX6aTZfpJv0
                  data_ga_name: SBOM and Attestation
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: Icône d'un e-book
                  event_type: "Livre"
                  header: "Guide sur la sécurité de la chaîne d'approvisionnement logicielle"
                  link_text: "En savoir plus"
                  image: "/nuxt-images/blogimages/modernize-cicd.jpg"
                  href: https://cdn.pathfactory.com/assets/10519/contents/360915/35d042c6-3449-4d50-b2e9-b08d9a68f7a1.pdf
                  data_ga_name: "Guide to software supply chain security"
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: Icône d'un e-book
                  event_type: "Livre"
                  header: "Enquête GitLab DevSecOps"
                  link_text: "En savoir plus"
                  image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
                  href: https://cdn.pathfactory.com/assets/10519/contents/432983/c6140cad-446b-4a6c-96b6-8524fac60f7d.pdf
                  data_ga_name: "GitLab DevSecOps survey"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Icône de blog
                  event_type: "Blog"
                  header: "L'importance de la conformité dans DevOps"
                  link_text: "En savoir plus"
                  href: https://about.gitlab.com/blog/2022/08/15/the-importance-of-compliance-in-devops/
                  image: /nuxt-images/blogimages/hotjar.jpg
                  data_ga_name: "The importance of compliance in DevOps"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Icône de blog
                  event_type: "Blog"
                  header: "Les 5 principales fonctionnalités de conformité à exploiter dans GitLab"
                  link_text: "En savoir plus"
                  href: https://about.gitlab.com/blog/2022/07/13/top-5-compliance-features-to-leverage-in-gitlab/
                  image: /nuxt-images/blogimages/cover_image_regenhu.jpg
                  data_ga_name: "Top 5 compliance features to leverage in GitLab"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    alt: Icône de blog
                    variant: marketing
                  event_type: "Blog"
                  header: "Comment faire respecter la séparation des tâches et assurer la conformité"
                  link_text: "En savoir plus"
                  href: "https://about.gitlab.com/blog/2022/04/04/ensuring-compliance/"
                  image: /nuxt-images/blogimages/scm-ci-cr.png
                  data_ga_name: "How to enforce separation of duties and achieve compliance"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    alt: Icône de blog
                    variant: marketing
                  event_type: "Blog"
                  header: "Ce que vous devez savoir sur les audits DevOps"
                  link_text: "En savoir plus"
                  href: "https://about.gitlab.com/blog/2022/08/31/what-you-need-to-know-about-devops-audits/"
                  image: /nuxt-images/blogimages/hemmersbach_case_study.jpg
                  data_ga_name: "What you need to know about DevOps Audits"
                  data_ga_location: resource cards
                - icon:
                    name: report
                    alt: Icône de rapport
                    variant: marketing
                  event_type: "Rapport d'analystes"
                  header: "GitLab obtient la position de Challenger dans le rapport Gartner Magic Quadrant 2022"
                  link_text: "En savoir plus"
                  href: "https://about.gitlab.com/analysts/gartner-ast22/"
                  image: /nuxt-images/blogimages/anwb_case_study_image_2.jpg
                  data_ga_name: "GitLab a challenger in 2022 Gartner Magic Quadrant"
                  data_ga_location: resource cards
    - name: solutions-cards
      data:
        title: Faites plus avec GitLab
        column_size: 4
        link :
          url: /solutions/
          text: Explorer d'autres solutions
          data_ga_name: solutions explore more
          data_ga_location: body
        cards:
          - title: DevSecOps
            description: GitLab permet à vos équipes de concilier rapidité et sécurité en automatisant la livraison de logiciels et en sécurisant votre chaîne d'approvisionnement logicielle de bout en bout.
            cta: En savoir plus
            icon:
              name: devsecops
              alt: Icône DevSecOps
              variant: marketing
            href: /solutions/security-compliance/
            data_ga_name: devsecpps learn more
            data_ga_location: body
          - title: Sécurité de la chaîne d'approvisionnement logicielle
            description: Assurez-vous que votre chaîne d'approvisionnement logicielle est sécurisée et conforme.
            cta: En savoir plus
            icon:
              name: continuous-delivery
              alt: Livraison continue
              variant: marketing
            href: /solutions/supply-chain/
            data_ga_name: software supply chain security learn more
            data_ga_location: body
          - title: Livraison automatisée de logiciels
            description: L'essentiel de l'automatisation pour réaliser l'innovation numérique, les transformations Cloud-natives et la modernisation des applications
            cta: En savoir plus
            icon:
              name: continuous-integration
              alt: Icône d'intégration continue
              variant: marketing
            href: /solutions/delivery-automation/
            data_ga_name: automated software delivery learn more
            data_ga_location: body

