---
  title: The benefits of GitOps workflows
  description:  Discover the benefits and best practices of GitOps workflows.
  topics_header:
      data:
        title: The benefits of GitOps workflows
        block:
            - text: |
                  GitOps is an operational framework that takes DevOps best practices and applies them to infrastructure automation. When teams use a GitOps workflow, they experience benefits across the development lifecycle.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: Gitops
      href: /topics/gitops/
      data_ga_name: topics gitops
      data_ga_location: breadcrumb
    - title: The benefits of GitOps workflows
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: A Git version control system enhances security and compliance
          href: "#a-git-version-control-system-enhances-security-and-compliance"
          data_ga_name: a git version control system enhances security and compliance
          data_ga_location: side-navigation
          variant: primary
        - text: Established best practices enhance collaboration and productivity
          href: "#established-best-practices-enhance-collaboration-and-productivity"
          data_ga_name: established best practices enhance collaboration and productivity
          data_ga_location: side-navigation
        - text: Automation improves the developer experience and reduces cost
          href: "#automation-improves-the-developer-experience-and-reduces-cost"
          data_ga_name: automation improves the developer experience and reduces cost
          data_ga_location: side-navigation
        - text: Continuous integrations leads to faster development and deployment
          href: "#continuous-integrations-leads-to-faster-development-and-deployment"
          data_ga_name: continuous integrations leads to faster development and deployment
          data_ga_location: side-navigation
        - text: Git workflows increase stability and reliability
          href: "#git-workflows-increase-stability-and-reliability"
          data_ga_name: git workflows increase stability and reliability
          data_ga_location: side-navigation
    hyperlinks:
      text: 'More on this topic'
      data: 
        - text: "What is GitOps?"
          href: /topics/gitops/
          variant: secondary
          data_ga_name: "what is GitOps"
          data_ga_location: "side-navigation"
    content:
      - name: 'topics-copy-block'
        data:
          header: A Git version control system enhances security and compliance
          column_size: 10
          blocks:
              - text: |
                  A simplified toolchain reduces attack surfaces, since teams use a single platform for infrastructure management. If an attack does occur, teams can revert to a desired state using the version control system. As a result, GitOps reduces downtime and outages, while enabling teams to continue development in an uncompromised environment.


                  Teams that must follow strict compliance often experience decreased [collaboration](/blog/2020/11/23/collaboration-communication-best-practices/){data-ga-name="collaboration" data-ga-location="body"} in heavily regulated contexts, where policy often limits the number of people who can enact changes to a production environment. With GitOps, however, anyone can propose a change via a [merge request](/blog/2021/03/18/iteration-and-code-review/){data-ga-name="merge request" data-ga-location="body"}, which widens the scope of collaboration while limiting the number of people with the ability to merge to the `production` branch.


                  When teams adopt a [GitOps workflow](/topics/gitops/gitops-workflow/){data-ga-name="gitops workflow" data-ga-location="body"}, they experience greater access control, because changes are automated using CI/CD tooling, eliminating the need to provide [access](/blog/2020/02/20/protecting-manual-jobs/){data-ga-name="access" data-ga-location="body"} credentials to all infrastructure components. GitOps empowers everyone to contribute, but greater collaboration accompanies a need to maintain a running history of all changes. GitOps ensures that all commits on the `main` [branch](/blog/2021/03/10/new-git-default-branch-name/){data-ga-name="main branch" data-ga-location="body"} act as a change log for auditing.

      - name: 'topics-copy-block'
        data:
          column_size: 10
          header: Established best practices enhance collaboration and productivity
          blocks:
              - text: |
                  GitOps incorporates software development best practices for [infrastructure as code](/topics/gitops/infrastructure-as-code/){data-ga-name="infrastructure as code" data-ga-location="body"}, Git [workflows](/blog/2020/04/07/15-git-tips-improve-workflow/){data-ga-name="workflows" data-ga-location="body"}, and CI/CD pipelines. Operations teams already have these pre-existing skills, knowledge, and toolchain requirements, so the decision to adopt GitOps won't result in a significant learning curve. GitOps workflows simplify processes to enhance visibility, create a single source of truth, and maintain a lean set of tools.


                  A GitOps workflow offers visibility and enhances collaboration, since teams use a Git version control system and merge requests as the mechanism for every infrastructure change. Every update goes through the same review and approval process, and teams can collaborate by sharing ideas, reviewing code, and offering feedback.
      - name: 'topics-copy-block'
        data:
          column_size: 10
          header: Automation improves the developer experience and reduces cost
          blocks:
              - text: |
                  With CI/CD tooling and continuous deployment, productivity increases, because teams benefit from automation and can focus on development rather than investing their efforts on tedious, manual tasks. GitOps workflows improve the developer experience since team members can use whichever language and tools they'd like before pushing updates to Git. There is a low barrier to entry, empowering anyone - from new hires to tenured team members - to become productive quickly and easily. Infrastructure automation improves productivity and reduces downtime, while facilitating better management of cloud resources, which can also decrease [costs](/blog/2020/10/27/how-we-optimized-our-infrastructure-spend-at-gitlab/){data-ga-name="costs" data-ga-location="body"}. Automating infrastructure definition and testing removes manual tasks and rework, while reducing downtimes due to built-in revert and rollback capabilities.
      - name: 'topics-copy-block'
        data:
          column_size: 10
          header: Continuous integrations leads to faster development and deployment
          blocks:
              - text: |
                  Teams have an easier time pushing a [minimum viable change](/blog/2020/11/09/lessons-in-iteration-from-new-infrastructure-team/){data-ga-name="minimum viable change" data-ga-location="body"}, since GitOps enables faster and more frequent deployments. Using GitOps best practices, teams can ship several times a day and revert changes if there is a problem. High velocity deployments lead to more rapid releases, helping teams deliver business and customer value. With continuous integration, teams are more agile and can quickly respond to customer needs.
      - name: 'topics-copy-block'
        data:
          column_size: 10
          header: Git workflows increase stability and reliability
          blocks:
              - text: |
                  Infrastructure is codified and repeatable, reducing human [error](/blog/2020/01/23/iteration-on-error-tracking/){data-ga-name="error" data-ga-location="body"}. Merge requests facilitate code reviews and collaboration, and they also help teams identify and correct errors before they make it to production. There is also less risk, since all changes to infrastructure are tracked through [merge requests](/blog/2020/12/14/merge-trains-explained/){data-ga-name="MRs" data-ga-location="body"}, and changes can be rolled back to a previous state if an iteration doesn't work out well. Git workflows reduce recovery time by enabling rollbacks to a more stable state and offering [distributed](https://git-scm.com/about/distributed) backup copies in the event of a serious outage. GitOps empowers teams to iterate faster to ship new features without the fear of causing an unstable environment.

  components:
    - name: solutions-resource-cards
      data:
        title: Ready to learn more about GitOps?
        column_size: 4
        grouped: false
        cards:
          - header: Discover how GitLab streamlines GitOps workflows
            link_text: Learn more
            image: "/nuxt-images/resources/resources_2.jpeg"
            event_type: Articles
            icon:
              name: article
              variant: marketing
              alt: article Icon
            href: /solutions/gitops/
            data_ga_name: discover how GitLab streamlines GitOps workflows
            data_ga_location: resources

          - header: Learn about the future of GitOps from tech leaders
            link_text: Watch now
            image: "/nuxt-images/resources/resources_10.jpeg"
            event_type: Webcast
            icon:
              name: webcast
              variant: marketing
              alt: Webcast Icon
            href: /why/gitops-infrastructure-automation/
            data_ga_name: Learn about the future of GitOps from tech leaders
            data_ga_location: resources
          - header: Download the beginner's guide to GitOps
            link_text: Learn more
            image: "/nuxt-images/resources/resources_3.jpg"
            event_type: Books
            icon:
              name: article
              variant: marketing
              alt: Book Icon
            href: https://page.gitlab.com/resources-ebook-beginner-guide-gitops.html
            data_ga_name: Download the beginner's guide to GitOps
            data_ga_location: resources
