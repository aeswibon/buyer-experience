---
  title: What is a GitOps workflow?
  description: GitOps has three main parts to its workflow, infrastructure as code, merge requests, and [CI/CD pipelines](/topics/ci-cd/cicd-pipeline/).
  topics_header:
      data:
        title: What is a GitOps workflow?
        block:
            - text: |
                  Managing IT infrastructure can be challenging, but teams that use well-known software development practices, including version control, code review, and CI/CD pipelines, find the process more convenient. By using config files, the same infrastructure environment is deployed each time. Many teams know that this workflow increases efficiency, collaboration, and stability, but they may wonder what it means to adopt GitOps.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: Gitops
      href: /topics/gitops/
      data_ga_name: topics gitops
      data_ga_location: breadcrumb
    - title: What is a GitOps workflow?
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: Three components of GitOps workflows
          href: "#three-components-of-git-ops-workflows"
          data_ga_name: three components of GitOps workflows
          data_ga_location: side-navigation
          variant: primary
    hyperlinks:
      text: 'More on this topic'
      data: 
        - text: "What is GitOps?"
          href: /topics/gitops/
          variant: secondary
          data_ga_name: "what is GitOps"
          data_ga_location: "side-navigation"
    content:
      - name: 'topics-copy-block'
        data:
          header: Three components of GitOps workflows
          column_size: 10
          blocks:
              - text: |
                  As a software development framework, GitOps has three main parts to its workflow, including infrastructure as code, merge requests, and CI/CD pipelines.

                  ### 1. Infrastructure as code (IaC)


                  The first step in a GitOps workflow is defining all [infrastructure as code](/topics/gitops/infrastructure-as-code/){data-ga-name="infrastructure as code" data-ga-location="body"}. IaC automates the IT infrastructure provisioning by using configuration files. IaC is a DevOps practice that supports teams to version infrastructure to improve consistency across machines to reduce deployment friction. Infrastructure code undergoes a similar process as application code with touchpoints in continuous integration, version control, testing, and continuous deployment. Automation leads to more [efficient](/blog/2020/04/17/why-gitops-should-be-workflow-of-choice/){data-ga-name="efficient" data-ga-location="body"} development, increased consistency, and [faster](/blog/2021/02/24/production-grade-infra-devsecops-with-five-minute-production/){data-ga-name="faster" data-ga-location="body"} time to market.


                  Managing [infrastructure](/blog/2020/11/09/lessons-in-iteration-from-new-infrastructure-team/){data-ga-name="infrastructure" data-ga-location="body"} has traditionally been a manual process involving large teams maintaining physical servers. Each machine often has its own configuration, leading to snowflake environments. With infrastructure as code, teams have increased visibility, consistency, stability, and scalability.


                  ### 2. Merge requests (MRs)


                  Declarative tools, such as Kubernetes, enable config files to be [version controlled](/blog/2020/11/12/migrating-your-version-control-to-git/){data-ga-name="version controlled" data-ga-location="body"} by Git, an open source version control system that tracks code changes. Using a Git repository as the single source of truth for infrastructure definitions, GitOps benefits from a robust audit trail. The second aspect of GitOps workflows involve merge requests, which serve as the [change function](/blog/2020/10/13/merge-request-reviewers/){data-ga-name="change function" data-ga-location="body"} for infrastructure updates.


                  Teams collaborate in merge requests through [code reviews](/blog/2021/01/25/mr-reviews-with-vs-code/){data-ga-name="code reviews" data-ga-location="body"}, comments, and suggestions. A merge commits to the [main](/blog/2021/03/10/new-git-default-branch-name/){data-ga-name="main" data-ga-location="body"} branch and acts as an audit log. Built-in rollback features enable teams to revert to a desired state and explore innovative ways to approach difficult challenges. Merge requests facilitate experimentation and provide a safe way for team members to receive fast [feedback](/blog/2021/03/18/iteration-and-code-review/){data-ga-name="feedback" data-ga-location="body"} from their peers and subject matter experts.


                  ### 3. Continuous integration and continuous deployment (CI/CD)


                  GitOps automates infrastructure management using a Git workflow with [effective](/blog/2020/07/29/effective-ci-cd-pipelines/){data-ga-name="effective" data-ga-location="body"} continuous integration and continuous deployment. After code is merged to the main branch, the CI/CD pipeline initiates the [change](/blog/2021/02/22/pipeline-editor-overview/){data-ga-name="change" data-ga-location="body"} in the environment. Manual changes and human error can cause configuration drift and snowflake environments, but GitOps automation and continuous deployment overwrites them so the environment always [deploys](/blog/2021/02/05/ci-deployment-and-environments/){data-ga-name="deploys" data-ga-location="body"} a consistent desired state.

  components:
    - name: solutions-resource-cards
      data:
        title: Ready to learn more about GitOps?
        column_size: 4
        grouped: false
        cards:
          - header: Discover how GitLab supports GitOps workflows
            link_text: Learn more
            image: "/nuxt-images/resources/resources_2.jpeg"
            event_type: Articles
            icon:
              name: article
              variant: marketing
              alt: article Icon
            href: /solutions/gitops/
            data_ga_name: discover how GitLab supports GitOps workflows
            data_ga_location: resources

          - header: Hear about the future of infrastructure automation
            link_text: Watch now
            image: "/nuxt-images/resources/resources_10.jpeg"
            event_type: Webcast
            icon:
              name: webcast
              variant: marketing
              alt: Webcast Icon
            href: /why/gitops-infrastructure-automation/
            data_ga_name: hear about the future of infrastructure automation
            data_ga_location: resources
          - header: Learn three ways to approach GitOps
            link_text: Learn more
            image: "/nuxt-images/resources/resources_3.jpg"
            event_type: Blog
            icon:
              name: blog
              variant: marketing
              alt: blog Icon
            href: /blog/2021/04/27/gitops-done-3-ways/
            data_ga_name: learn three ways to approach GitOps
            data_ga_location: resources
