---
  title: How continuous integration and continuous delivery work together
  description: Learn how continuous integration and continuous delivery use CI/CD pipelines to help teams deploy faster.
  topics_header:
    data:
      title: How continuous integration and continuous delivery work together
      block:
        - metadata:
            id_tag: cicd-pipeline
          text: |
            Learn how continuous integration and continuous delivery use CI/CD pipelines to help teams deploy faster.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: CI CD
      href: /topics/ci-cd/
      data-ga-name: ci-cd
      data_ga_location: breadcrumb
    - title: How continuous integration and continuous delivery work together
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: 'CI/CD pipelines'
          href: "#ci-cd-pipelines"
          data_ga_name: CI/CD pipelines
          data_ga_location: side-navigation
          variant: primary
        - text: 'How continuous delivery works'
          href: "#how-continuous-delivery-works"
          data_ga_name: How continuous delivery works
          data_ga_location: side-navigation
    content:
      - name: topics-copy-block
        data:
          no_header: true
          column_size: 10
          blocks:
            - text: |
                CI/CD helps DevOps teams release better quality software, faster. Continuous integration and continuous delivery do this by automating the way teams build, package, test, and deploy applications to users. 

                Continuous integration encourages teams to commit small, frequent code changes to a central repository multiple times a day. All code changes are continually validated against other code changes integrated into the same repository. 

      - name: topics-copy-block
        data:
          header: CI/CD pipelines 
          column_size: 10
          blocks:
            - text: |
                  Code is tested at each stage through [automated CI/CD pipelines](/topics/ci-cd/cicd-pipeline/){data-ga-name="automated CI/CD pipelines" data-ga-location="body"}. If all jobs in a pipeline pass their tests, the pipeline proceeds to the next stage. If any job in a pipeline fails, the pipeline stops early. Developers can then fix the problems found in the tests, and with smaller code changes, this is a much easier task. CI/CD pipelines maintain the integrity of the shared repository because they ensure that only code that meets certain standards is integrated.

                  Once all code has passed the quality and security testing, it's ready to be released. This is where continuous delivery takes over.

                  [Continuous delivery](/stages-devops-lifecycle/continuous-delivery/){data-ga-name="continuous delivery" data-ga-location="body"} automates the application release process for CI-validated code. Continuous delivery can cover everything from provisioning the infrastructure environment to deploying the tested application to test/staging or production environments. Continuous delivery uses pipelines to make sure that the application code is packaged with everything it needs to deploy to [any environment](/topics/multicloud/){data-ga-name="multicloud" data-ga-location="body"} you choose. 

      - name: topics-copy-block
        data:
          header: How continuous delivery works
          column_size: 10
          blocks:
            - text: |
                The goal for organizations practicing continuous delivery is to make the software release process automated and repeatable (i.e. [boring](https://about.gitlab.com/blog/2020/08/18/boring-solutions-faster-iteration/){data-ga-name="boring" data-ga-location="body"}). CD allows teams to plan their release processes and schedules, automate infrastructure and deployments, and manage their [cloud resources](https://devops.com/cloud-and-devops-ci-cd-and-market-analysis/) more effectively. 


                Deployment environments and architectures have grown more complex as development velocity has increased. Teams working in cloud environments may use containers and rely on orchestration tools like Kubernetes to ship applications. Other teams may have adopted a [microservices](/topics/microservices/){data-ga-name="microservices" data-ga-location="body"} architecture. Continuous delivery needs to accommodate a vast array of deployment scenarios in order to be successful. This is why releases are often a bottleneck in the software delivery process.


                Continuous integration and continuous delivery are the software processes that embody the DevOps philosophy. In the [ideal DevOps](/topics/devops/build-a-devops-team/){data-ga-name="devops team" data-ga-location="body"} team structure, there is visibility and communication throughout the software development lifecycle. Information silos are limited so that dev and ops can work together. Similarly, CI can represent devs and CD can represent ops.


                CI/CD is a DevOps best practice because it addresses the challenge between developers who want to move fast, with operations that want stability and reliability. With CI/CD automation, developers can push changes more frequently. Operations teams see greater stability because environments have standard configurations, there is continuous testing in the delivery process, and releases are repeatable and predictable.
  components:
    - name: topics-cta
      data:
        subtitle: GitLab CI/CD
        text: Powerful automation to build and test faster at any scale
        column_size: 10
        cta_one:
          text: Learn More
          link: /solutions/continuous-integration/
          data_ga_name: Learn More
          data_ga_location: body
    - name: solutions-resource-cards
      data:
        title: Continuous integration best practices
        column_size: 4
        cards:
          - icon:
              name: web
              variant: marketing
              alt: web Icon
            event_type: "Web"
            header: Continuous delivery of a Spring Boot application with GitLab CI and Kubernetes
            image: "/nuxt-images/resources/resources_18.jpg"
            link_text: "Learn more"
            href: /blog/2016/12/14/continuous-delivery-of-a-spring-boot-application-with-gitlab-ci-and-kubernetes/
            data_ga_name: Continuous delivery of a Spring Boot application with GitLab CI and Kubernetes
            data_ga_location: body
    - name: solutions-resource-cards
      data:
        title: Suggested Content
        column_size: 4
        cards:
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: Why GitLab CI/CD?
            text: |
                  With GitLab's out-of-the-box CI/CD, you can spend less time maintaining and more time creating.
            link_text: "Learn more"
            href: /blog/2019/04/02/why-gitlab-ci-cd/
            image: /nuxt-images/blogimages/autodevops.jpg
            data_ga_name: Why GitLab CI/CD
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "A beginner's guide to continuous integration"
            text: |
                  Here's how to help everyone on your team, like designers and testers, get started with GitLab CI.
            link_text: "Learn more"
            href: /blog/2018/01/22/a-beginners-guide-to-continuous-integration/
            image: /nuxt-images/blogimages/conversicaimage.jpg
            data_ga_name: "A beginner's guide to continuous integration"
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: The business impact of CI/CD
            text: |
                  How a good CI/CD strategy generates revenue and keeps developers happy.
            link_text: "Learn more"
            href: /blog/2019/06/21/business-impact-ci-cd/
            image: /nuxt-images/blogimages/agilemultipleteams.jpg
            data_ga_name: The business impact of CI/CD
            data_ga_location: resource cards
