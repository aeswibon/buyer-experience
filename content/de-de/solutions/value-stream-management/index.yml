---
  title: Value Stream Management
  description: Das Value Stream Management von GitLab ist eine systematische Methode, um die Zeit bis zur Wertschöpfung zu verkürzen, die Geschäftsergebnisse zu optimieren und die Softwarequalität zu verbessern.
  report_cta:
    layout: "dark"
    title: Analystenberichte
    reports:
    - description: "GitLab wird als einziger Leader in The Forrester Wave™ aufgeführt: Integrated Software Delivery Platforms, Q2 2023."
      link_text: Lesen Sie den Bericht
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
    - description: "GitLab wird als Leader im Gartner® Magic Quadrant™ für DevOps-Plattformen 2023 aufgeführt."
      link_text: Lesen Sie den Bericht      
      url: /gartner-magic-quadrant/
  components:
    - name: 'solutions-hero'
      data:
        title: Value Stream Management
        subtitle: Messe und verwalte den Geschäftswert deines DevSecOps-Lebenszyklus.
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Kostenlosen Testzeitraum starten
          url: /free-trial/
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Bild: gitlab + Value Stream Management"
    - name: copy-media
      data:
        block:
          - header: Vorteile des Value Stream Management
            miscellaneous: |
              Die Softwareentwicklung sollte immer darauf abzielen, den Wert für den Kunden oder das Unternehmen zu maximieren. Aber wie erkennst du Ineffizienzen bei der Bereitstellung und wie kannst du den Kurs korrigieren, wenn du sie erkennst? Das Value Stream Management von GitLab hilft Unternehmen dabei, ihre durchgängigen DevSecOps-Workstreams zu visualisieren, Verschwendung und Ineffizienzen zu erkennen und zu beseitigen sowie Maßnahmen zu ergreifen, um diese Workstreams so zu optimieren, dass sie die höchstmögliche Geschwindigkeit der Wertschöpfung liefern.
          - header: Eine Value Stream-Delivery-Plattform
            miscellaneous: |
              In der Studie [The Future of DevOps Toolchains Will Involve Maximizing Flow in IT Value Streams](https://www.gartner.com/en/documents/3979558/the-future-of-devops-toolchains-will-involve-maximizing){data-ga-name="The Future of DevOps Toolchains Will Involve Maximizing Flow in IT Value Streams" data-ga-location="body"} empfiehlt Gartner, dass „Infrastruktur- und Betriebsleiter, die für die Auswahl und den Einsatz von DevOps-Toolchains verantwortlich sind, die Geschäftsfähigkeit steigern sollten, indem sie DevOps-Plattformen zur Bereitstellung von Value Streams nutzen, die den Aufwand für die Verwaltung komplexer Toolchains reduzieren.“ [1] Durch die Bereitstellung einer [umfassenden DevOps-Plattform](/solutions/devops-platform/){data-ga-name="GitLab – The DevOps Platform" data-ga-location="body"} als eine einzige Anwendung ist GitLab einzigartig geeignet, um End-to-End-Transparenz während des gesamten Lebenszyklus ohne die „Toolchain-Steuer“ zu bieten. Als der Ort, an dem die Arbeit stattfindet, kann GitLab auch die Visualisierung mit der Aktion verbinden, so dass die Benutzer/innen jederzeit vom Lernen zum Handeln übergehen können, ohne den Kontext zu verlieren.

              *[1] Gartner „The Future of DevOps Toolchains Will Involve Maximizing Flow in IT Value Streams“, Manjunath Bhat, et al, 14. Januar 2020 (Gartner-Abonnement erforderlich)*
            media_link_href: https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html
            media_link_text: Value Stream Analytics
            media_link_data_ga_name: Value Stream Analytics
            media_link_data_ga_location: body
          - header: End-to-End-Prozesse anzeigen und verwalten
            text: |
              Value Stream Analytics helfen dir dabei, den DevSecOps-Fluss von der Idee bis zur Auslieferung an den Kunden zu visualisieren und zu verwalten. GitLab bietet sofort umsetzbare Berichte über gängige Workflows und Metriken, ohne dass du etwas installieren oder konfigurieren musst. Wenn du tiefer eintauchen oder benutzerdefinierte Workflows modellieren möchtest, kannst du mit dem einheitlichen, umfassenden Datenspeicher von GitLab alle wichtigen Ereignisse leicht nachverfolgen.

              * **Ein Tool, keine Kette:** GitLab vereint die gesamte DevSecOps-Toolchain in einer einzigen Anwendung. Es gibt keine zu verwaltenden Integrationen, keine API-Blockaden, die die Sichtbarkeit einschränken, und eine gemeinsame Erfahrung für alle im Unternehmen, unabhängig von ihrer Rolle.

              * **Gemeinsame, umsetzbare Daten:** Eine einzige Erkenntnisquelle, die auf einem einzigen Arbeitssystem aufbaut, bedeutet, dass du mehr Zeit damit verbringen kannst, Werte zu entwickeln, und weniger Zeit damit, herauszufinden, wo sie stecken geblieben sind.

              * **Fokus auf den Wert – nicht auf die Zeremonie:** Verfolge und verwalte den tatsächlichen Arbeitsfluss und eliminiere unnötige Zeremonien. Mit einer einzigen DevSecOps-Anwendung kannst du mit nur einem Klick auf die tatsächlichen Arbeitsaufgaben zugreifen und so Blockaden beseitigen, sobald du sie entdeckst.

            video:
              video_url: https://www.youtube.com/embed/8pLEucNUlWI
            inverted: true
          - header: Messen
            text: |
              Treibe die kontinuierliche Verbesserung auf der Grundlage der Daten aus deinem Value Stream an.

              * **Value Stream verfolgen und beschleunigen:** Die Konzentration auf den Value Stream zum Kunden ist der Schlüssel zur Rationalisierung des Lieferprozesses.
              * **Zykluszeit:** Verfolge die tatsächliche Arbeit und den Aufwand – die reale Zykluszeit für die Wertschöpfung.
              * **Geschäftswert:** Verknüpfe den tatsächlichen Wert mit der Veränderung.
              * **DORA4-Kennzahlen:** [DORA4-Kennzahlen](https://about.gitlab.com/solutions/value-stream-management/dora/) helfen dir beim Benchmarking deines DevSecOps-Reifegrads und sind ein nützlicher Indikator für den Vergleich – entweder innerhalb des Teams oder mit vergleichbaren Unternehmen/Branchen. Beobachte die [Vorlaufzeit für Änderungen](https://docs.gitlab.com/ee/api/dora4_project_analytics.html#list-project-merge-request-lead-times){data-ga-name="DORA4 Analytics Project API | GitLab" data-ga-location="body"} und [Deployment Frequency](https://docs.gitlab.com/ee/api/dora4_project_analytics.html){data-ga-name="DORA4 Analytics Project API | GitLab" data-ga-location="body"}, um die Effizienz deiner DevSecOps-Prozesse zu messen.
            image:
              image_url: /nuxt-images/solutions/tasks-by-type.png
              alt: ""
              caption: |
                [Sehen Sie sich die Verteilung des Geschäftswerts über die einzelnen Arbeitsarten an, um die Umsetzung besser an der Strategie auszurichten.](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html#type-of-work---tasks-by-type-chart){data-ga-name="Value Stream Analytics | GitLab" data-ga-location="body"}
          - header: Verwalten
            text: |
              Mit Value Streams kannst du den Fluss neuer Innovationen von der Idee bis zum Kunden visualisieren und verwalten.

              * **Einschränkungen identifizieren**: Finde die Reibungspunkte in deinem Value Stream und optimiere die Durchlaufzeit.
              * **Entferne Engpässe:** Ergreife Maßnahmen zur Beseitigung von Engpässen.
              * **Fluss optimieren:** Beschleunige die DevSecOps Best Practices in deinem Unternehmen.
              * **Effizienz verwalten:** [Detaillierte Einblicke](https://docs.gitlab.com/ee/user/project/insights/){data-ga-name="Value Stream Analytics | GitLab" data-ga-location="body"}, die deine Triage-Hygiene, die erstellten Tickets/Bugs, die Zeit bis zum Merge und vieles mehr zeigen, ermöglichen es dir, Ineffizienzen in deinem Entwicklungsprozess leicht zu erkennen und zu beheben.
            image:
              image_url: /nuxt-images/solutions/days-to-complete.png
              alt: ""
              caption: |
                [Untersuchen und verbessern Sie Prozesse, erkennen Sie Trends und ermitteln Sie Ausreißer anhand der Ursachen.](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html#days-to-completion-chart){data-ga-name="Insights | GitLab" data-ga-location="body"}
              vertical_centered: true
            inverted: true
    - name: 'solutions-carousel-videos'
      data:
        title: Value Stream Management
        videos:
          - title: "VSM-Einblicke für Führungskräfte"
            photourl: /nuxt-images/video-carousels/vsm_1/1.jpg
            video_link: https://www.youtube-nocookie.com/embed/0MR7Q8Wiq6g
            carousel_identifier:
              - ''

          - title: "VSM-Einblicke für Entwickler(innen)"
            photourl: /nuxt-images/video-carousels/vsm_1/4.jpg
            video_link: https://www.youtube-nocookie.com/embed/JPYWrRByAYk
            carousel_identifier:
              - ''

          - title: "VSM-Einblicke in die Sicherheit"
            photourl: /nuxt-images/video-carousels/vsm_1/5.jpg
            video_link: https://www.youtube-nocookie.com/embed/KSJ9VhKQAS0
            carousel_identifier:
              - ''

          - title: "VSM-Einblicke in den Betrieb"
            photourl: /nuxt-images/video-carousels/vsm_1/4.jpg
            video_link: https://www.youtube-nocookie.com/embed/lDWxH2YO3Yk
            carousel_identifier:
              - ''

          - title: "VSM-Einblicke in die Produktivität"
            photourl: /nuxt-images/video-carousels/vsm_1/5.jpg
            video_link: https://www.youtube-nocookie.com/embed/vYKC7r8ztVA
            carousel_identifier:
              - ''

          - title: "VSM-Ticket-Boards für Mapping"
            photourl: /nuxt-images/video-carousels/vsm_1/6.jpg
            video_link: https://www.youtube-nocookie.com/embed/9ASHiQ2juYY
            carousel_identifier:
              - ''

          - title: "VSM-Überwachung des Geschäftswerts"
            photourl: /nuxt-images/video-carousels/vsm_1/7.jpg
            video_link: https://www.youtube-nocookie.com/embed/oG0VESUOFAI
            carousel_identifier:
              - ''
