---
title: 'Durchstarten mit Enterprise'
description: 'Mit dieser Anleitung können Sie schnell die Grundlagen für die automatisierte Softwareentwicklung und -bereitstellung im Premium-Plan einrichten.'
side_menu:
  anchors:
    text: 'Auf dieser Seite'
    data:
      - text: Erste Schritte
        href: '#getting-started'
        data_ga_name: getting-started
        data_ga_location: side-navigation
      - text: Einrichtung
        href: '#getting-setup'
        data_ga_name: getting-setup
        data_ga_location: side-navigation
      - text: GitLab verwenden
        href: '#using-gitlab'
        data_ga_name: using-gitlab
        data_ga_location: side-navigation
  hyperlinks:
    text: ''
    data: []
hero:
  crumbs:
    - title: Durchstarten
      href: /get-started/
      data_ga_name: Get Started
      data_ga_location: breadcrumb
    - title: Durchstarten mit Enterprise
  header_label: 'Lesedauer: 20 Minuten'
  title: Durchstarten mit Enterprise
  content: |
    Um wettbewerbsfähig zu bleiben, brauchen Sie eine Möglichkeit, DevSecOps zu vereinfachen und zu skalieren, damit Ihre Teams sicheren Code schneller bereitstellen können. Mit dieser Anleitung können Sie schnell die Grundlagen für die automatisierte Softwareentwicklung und -bereitstellung im Premium-Plan einrichten. Die Optionen für Sicherheit, Konformität und Projektplanung erhalten Sie mit dem Ultimate-Plan.
steps:
  text:
    show: Alle anzeigen
    hide: Alle ausblenden
  groups:
    - header: Erste Schritte
      id: getting-started
      items:
        - title: Wählen Sie das richtige Abonnement für Ihre Bedürfnisse
          copies:
            - title: GitLab SaaS oder GitLab Self-Managed
              text: |
                <p>Möchten Sie Ihre GitLab-Plattform von GitLab verwalten lassen oder möchten Sie sie selbst verwalten?</p>
              link:
                href: https://docs.gitlab.com/ee/subscriptions/#choose-between-gitlab-saas-or-gitlab-self-managed
                text: Unterschiede ansehen
                ga_name: GitLab Saas vs Self-Managed
                ga_location: body
        - title: Entscheiden Sie, welche Stufe Ihren Bedürfnissen entspricht
          copies:
            - title: Premium oder Ultimate
              text: |
                <p>Um herauszufinden, welche Stufe die richtige ist, sollten Sie Folgendes beachten: </p>

            - title: Benötigte Speichermenge
              text: |
                <p>Namensräume der Premium-Stufe auf GitLab SaaS haben einen Grenzwert von 50 GB und Namensräume der Ultimate-Stufe können bis zu 250 GB speichern.</p>
            - title: Gewünschte Sicherheit und Konformität
              text: |
                <p>* In der Premium-Stufe sind die Erkennung von Geheimnissen, SAST und das Scannen von Containern verfügbar.</p>
                <p>* Zusätzliche Scanner <a href="https://docs.gitlab.com/ee/user/application_security/">wie DAST, Abhängigkeiten, Cluster-Images, IaC, APIs und Fuzzing</a> sind in Ultimate verfügbar.</p>
                <p>* Umsetzbare Ergebnisse, die in die Pipeline für die Zusammenführung von Anforderungen und das Sicherheits-Dashboard integriert sind, erfordern das in Ultimate verfügbare Schwachstellenmanagement.</p>
                <p>* Konformitätspipelines erfordern Ultimate.</p>
                <p>* Lesen Sie mehr über unsere <a href="https://docs.gitlab.com/ee/user/application_security/">Sicherheitsscanner </a> und <a href="https://docs.gitlab.com/ee/administration/compliance.html">unsere Konformitätsfunktionen</a>.
              link:
                href: /pricing/
                text: Auf unsere Preise-Seite finden Sie weitere Informationen
                ga_name: pricing
                ga_location: body
    - header: Einrichtung
      id: getting-setup
      items:
        - title: Richten Sie Ihr SaaS-Abonnementkonto ein
          copies:
            - title: Bestimmen Sie, wie viele Sitzplätze Sie wünschen
              text: |
                Ein GitLab SaaS-Abonnement verwendet ein gleichzeitiges (Sitz-)Modell. Sie bezahlen ein Abonnement entsprechend der maximalen Anzahl von Benutzern, die der obersten Gruppe oder ihren untergeordneten Benutzern während des Abrechnungszeitraums zugewiesen sind. Sie können während des Abonnementzeitraums Benutzer hinzufügen und entfernen, solange die Gesamtzahl der Benutzer zu einem bestimmten Zeitpunkt die Anzahl der Abonnements nicht überschreitet.
              link:
                href: https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#how-seat-usage-is-determined
                text: Mehr erfahren
                ga_name: Determine how many seats you want
                ga_location: body
            - title: Besorgen Sie sich Ihr SaaS-Abonnement
              text: |
                <p>GitLab SaaS ist das Software-as-a-Service-Angebot von GitLab, das auf GitLab.com verfügbar ist. Sie müssen nichts installieren, um GitLab SaaS zu nutzen, Sie müssen sich nur anmelden. Das Abonnement bestimmt, welche Funktionen für Ihre privaten Projekte verfügbar sind. Organisationen mit öffentlichen Open-Source-Projekten können sich aktiv für unser GitLab for Open Source-Programm bewerben.<p/>

                <p>Funktionen von <a href="/pricing/ultimate/">GitLab Ultimate</a>, einschließlich 50.000 Minuten berechnen, stehen qualifizierten Open-Source-Projekten über das <a href="/solutions/open-source/">GitLab for Open Source</a> Programm kostenlos zur Verfügung.</p>
              link:
                href: https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#view-your-gitlabcom-subscription
                text: Mehr erfahren
                ga_name: Obtain your SaaS subscription
                ga_location: body
            - title: Bestimmen Sie die benötigten CI/CD-Shared-Runner-Minuten
              text: |
                <p><a href="https://docs.gitlab.com/ee/ci/runners/runners_scope.html#shared-runners" data-ga-name="Shared Runners" data-ga-location="body">Gemeinsame Läufer</a> werden mit jedem Projekt und jeder Gruppe in einer GitLab-Instanz geteilt. Wenn Jobs auf gemeinsam genutzten Läufern ausgeführt werden, werden Minuten berechnen verwendet. Auf GitLab.com wird das Kontingent an Minuten berechnen für jeden <a href="https://docs.gitlab.com/ee/user/namespace/" data-ga-name="namespaces" data-ga-location="body">Namespace</a>festgelegt und durch <a href="/pricing/" data-ga-name="Your license tier" data-ga-location="body">Ihre Lizenzstufe bestimmt.</a><p/>

                <p>Zusätzlich zum monatlichen Kontingent können Sie auf GitLab.com bei Bedarf <a href="https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#purchase-additional-cicd-minutes" data-ga-name="purchase additional compute minutes\" data-ga-location="body">zusätzliche Minuten berechnen erwerben</a> .</p>
        - title: Richten Sie Ihr selbstverwaltetes Abonnementkonto ein
          copies:
            - title: Bestimmen Sie, wie viele Sitzplätze Sie wünschen
              text: |
                Ein GitLab SaaS-Abonnement verwendet ein gleichzeitiges (Sitz-)Modell. Sie bezahlen ein Abonnement entsprechend der maximalen Anzahl von Benutzern, die der obersten Gruppe oder ihren untergeordneten Benutzern während des Abrechnungszeitraums zugewiesen sind. Sie können während des Abonnementzeitraums Benutzer hinzufügen und entfernen, solange die Gesamtzahl der Benutzer zu einem bestimmten Zeitpunkt die Anzahl der Abonnements nicht überschreitet.
              link:
                href: https://docs.gitlab.com/ee/subscriptions/self_managed/#subscription-seats
                text: Mehr erfahren
                ga_name: Determine how many seats you want
                ga_location: body
            - title: Erhalten Sie Ihr selbstverwaltetes Abonnement
              text: |
                Sie können Ihre eigene GitLab-Instanz installieren, verwalten und warten. Für die GitLab-Abonnementverwaltung ist Zugriff auf das Kundenportal erforderlich.
              link:
                href: https://docs.gitlab.com/ee/subscriptions/self_managed/
                text: Mehr erfahren
                ga_name: Determine how many seats you want
                ga_location: body
            - title: Aktivieren Sie GitLab Enterprise Edition
              text: |
                Wenn Sie eine neue GitLab-Instanz ohne Lizenz installieren, sind nur kostenlose Funktionen aktiviert. Um weitere Funktionen in GitLab Enterprise Edition (EE) zu aktivieren, aktivieren Sie Ihre Instanz mit einem Aktivierungscode.
              link:
                href: https://docs.gitlab.com/ee/user/admin_area/license.html
                text: Mehr erfahren
                ga_name: Activate GitLab Enterprise Edition
                ga_location: body
            - title: Überprüfen Sie die Systemanforderungen
              text: |
                Sehen Sie sich sowohl die unterstützten Betriebssysteme als auch die Mindestanforderungen an, die für die Installation und Verwendung von GitLab erforderlich sind.
              link:
                href: https://docs.gitlab.com/ee/install/requirements.html
                text: Mehr erfahren
                ga_name: Review the system requirements
                ga_location: body
            - title: Installieren Sie GitLab
              text: |
                <p>Wählen Sie Ihre <a href="https://docs.gitlab.com/ee/install/#choose-the-installation-method" data-ga-name="Installation Method" data-ga-location="body">Installationsmethode</a></p>

                <p>Installieren Sie es bei <a href="https://docs.gitlab.com/ee/install/#install-gitlab-on-cloud-providers" data-ga-name="your cloud provider" data-ga-location="body">Ihrem Cloud-Anbieter</a> (falls zutreffend)</p>
            - title: Konfigurieren Sie Ihre Instanz
              link:
                href: https://docs.gitlab.com/ee/install/next_steps.html
                text: Mehr erfahren
                ga_name: Configure your instance
                ga_location: body
            - title: Richten Sie eine Offline-Umgebung ein
              text: |
                Richten Sie eine Offline-Umgebung ein, wenn eine Isolierung vom öffentlichen Internet erforderlich ist (gilt normalerweise für regulierte Branchen).
              link:
                href: https://docs.gitlab.com/ee/user/application_security/offline_deployments/index.html
                text: Mehr erfahren
                ga_name: Set up off-line environment
                ga_location: body
            - title: Erwägen Sie, die zulässigen Minuten für gemeinsam genutzte CI/CD-Runner zu begrenzen
              text: |
                Um die Ressourcennutzung auf selbstverwalteten GitLab-Instanzen zu steuern, kann das Kontingent an Minuten berechnen für jeden Namespace von Administratoren festgelegt werden.
              link:
                href: https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#set-the-quota-of-cicd-minutes-for-a-specific-namespace
                text: Mehr erfahren
                ga_name: Consider limiting CI/CD shared runner minutes allowed
                ga_location: body
            - title: Installieren Sie den GitLab-Runner
              text: |
                GitLab Runner kann unter GNU/Linux, macOS, FreeBSD und Windows installiert und verwendet werden.
              link:
                href: https://docs.gitlab.com/runner/install/
                text: Mehr erfahren
                ga_name: Install GitLab runner
                ga_location: body
            - title: GitLab-Runner konfigurieren (optional)
              text: |
                GitLab Runner kann unter GNU/Linux, macOS, FreeBSD und Windows installiert und verwendet werden.
              link:
                href: https://docs.gitlab.com/runner/configuration/
                text: Mehr erfahren
                ga_name: Configure GitLab runner
                ga_location: body
            - title: Verwaltung
              text: |
                Selbstverwaltung erfordert Selbstverwaltung
              link:
                href: https://docs.gitlab.com/ee/administration/
                text: Mehr erfahren
                ga_name: Self Administration
                ga_location: body
        - title: Anwendungen integrieren (optional)
          copies:
            - text: |
                Sie können Funktionen wie Geheimnisverwaltung oder Authentifizierungsdienste hinzufügen oder etablierte Anwendungen wie Issue-Tracker integrieren.
              link:
                href: https://docs.gitlab.com/ee/integration/
                text: Mehr erfahren
                ga_name: Integrate applications
                ga_location: body
    - header: GitLab verwenden
      id: using-gitlab
      items:
        - title: Richten Sie Ihre Organisation ein
          copies:
            - text: |
                Konfigurieren Sie Ihre Organisation und ihre Benutzer. Bestimmen Sie Benutzerrollen und gewähren Sie jedem Zugriff auf die Projekte, die er benötigt.
              link:
                href: https://docs.gitlab.com/ee/topics/set_up_organization.html
                text: Mehr erfahren
                ga_name: Setup your organization
                ga_location: body
        - title: Organisieren Sie die Arbeit mit Projekten
          copies:
            - text: |
                In GitLab können Sie Projekte erstellen, um Ihre Codebasis zu hosten. Sie können Projekte auch verwenden, um Probleme zu verfolgen, Arbeit zu planen, am Code zusammenzuarbeiten und kontinuierlich zu erstellen, zu testen und das integrierte CI/CD zum Bereitstellen Ihrer App zu verwenden.
              link:
                href: https://docs.gitlab.com/ee/user/project/index.html
                text: Mehr erfahren
                ga_name: Organize work with projects
                ga_location: body
        - title: Planen und verfolgen Sie die Arbeit
          copies:
            - text: |
                Planen Sie Ihre Arbeit, indem Sie Anforderungen, Probleme und Epics erstellen. Planen Sie die Arbeit mit Meilensteinen und verfolgen Sie die Zeit Ihres Teams. Erfahren Sie, wie Sie mit schnellen Aktionen Zeit sparen, sehen Sie, wie GitLab Markdown-Text rendert, und erfahren Sie, wie Sie Git für die Interaktion mit GitLab verwenden.
              link:
                href: https://docs.gitlab.com/ee/topics/plan_and_track.html
                text: Mehr erfahren
                ga_name: Plan and track work
                ga_location: body
        - title: Erstellen Sie Ihre Anwendung
          copies:
            - text: |
                Fügen Sie Ihren Quellcode einem Repository hinzu, erstellen Sie Zusammenführungsanforderungen, um Code einzuchecken, und verwenden Sie CI/CD, um Ihre Anwendung zu generieren.
              link:
                href: https://docs.gitlab.com/ee/topics/build_your_application.html
                text: Mehr erfahren
                ga_name: Build your application
                ga_location: body
        - title: Sichern Sie Ihre Bewerbung
          copies:
            - title: Bestimmen Sie, welche Scanner Sie verwenden möchten (alle sind standardmäßig aktiviert).
              text: |
                GitLab bietet sowohl Container-Scanning als auch Abhängigkeits-Scanning, um die Abdeckung aller dieser Abhängigkeitstypen sicherzustellen. Um einen möglichst großen Teil Ihres Risikobereichs abzudecken, empfehlen wir Ihnen, alle unsere Sicherheitsscanner zu verwenden.
              link:
                href: https://docs.gitlab.com/ee/user/application_security/configuration/
                text: Mehr erfahren
                ga_name: Determine which scanners you’d like to use
                ga_location: body
            - title: Konfigurieren Sie Ihre Sicherheitsrichtlinien
              text: |
                Richtlinien in GitLab bieten Sicherheitsteams die Möglichkeit, die Ausführung von Scans ihrer Wahl zu verlangen, wenn eine Projektpipeline gemäß der angegebenen Konfiguration ausgeführt wird. Sicherheitsteams können daher sicher sein, dass die von ihnen eingerichteten Scans nicht geändert, verändert oder deaktiviert wurden.
              link:
                href: https://docs.gitlab.com/ee/user/application_security/policies/
                text: Mehr erfahren
                ga_name: Configure your security policies
                ga_location: body
            - title: Konfigurieren Sie MR-Genehmigungsregeln und Sicherheitsgenehmigungen
              text: |
                Mit Genehmigungsregeln für Zusammenführungsanfragen können Sie die Mindestanzahl erforderlicher Genehmigungen festlegen, bevor Arbeiten in Ihr Projekt integriert werden können. Sie können diese Regeln auch erweitern, um zu definieren, welche Benutzertypen Arbeit genehmigen können.
              link:
                href: https://docs.gitlab.com/ee/user/project/merge_requests/approvals/
                text: Mehr erfahren
                ga_name: Configure MR approval rules and security approvals
                ga_location: body
        - title: Stellen Sie Ihre Anwendung bereit und geben Sie sie frei
          copies:
            - text: |
                Stellen Sie Ihre Anwendung intern oder öffentlich bereit. Verwenden Sie Flags, um Funktionen schrittweise freizugeben.
              link:
                href: https://docs.gitlab.com/ee/topics/release_your_application.html
                text: Mehr erfahren
                ga_name: Deploy and release your application
                ga_location: body
        - title: Überwachen Sie die Anwendungsleistung
          copies:
            - text: |
                GitLab bietet eine Vielzahl von Tools, die Sie beim Betrieb und der Wartung Ihrer Anwendungen unterstützen.  Sie können die Kennzahlen verfolgen, die für Ihr Team am wichtigsten sind, automatische Warnungen generieren, wenn die Leistung nachlässt, und diese Warnungen verwalten – alles in GitLab.
              link:
                href: https://docs.gitlab.com/ee/operations/index.html
                text: Mehr erfahren
                ga_name: Monitor application performance
                ga_location: body
        - title: Überwachen Sie die Läuferleistung
          copies:
            - text: |
                GitLab verfügt ab GitLab 8.4 über ein eigenes System zur Messung der Anwendungsleistung, genannt „GitLab Performance Monitoring“. GitLab Performance Monitoring ermöglicht die Messung verschiedenster Statistiken
              link:
                href: https://docs.gitlab.com/runner/monitoring/index.html
                text: Mehr erfahren
                ga_name: Monitor runner performance
                ga_location: body
        - title: Verwalten Sie Ihre Infrastruktur
          copies:
            - text: |
                Mit dem Aufkommen von DevSecOps- und SRE-Ansätzen wird das Infrastrukturmanagement kodifiziert und automatisierbar, und Best Practices für die Softwareentwicklung gewinnen auch im Infrastrukturmanagement ihren Platz. GitLab bietet verschiedene Funktionen, um Ihre Infrastrukturverwaltungspraktiken zu beschleunigen und zu vereinfachen.
              link:
                href: https://docs.gitlab.com/ee/user/infrastructure/index.html
                text: Mehr erfahren
                ga_name: Manage your infrastructure
                ga_location: body
        - title: Analysieren Sie die GitLab-Nutzung
          copies:
            - text: |
                Dies misst, wie oft Sie Endbenutzern einen Mehrwert bieten. Eine höhere Bereitstellungshäufigkeit bedeutet, dass Sie früher Feedback erhalten und schneller iterieren können, um Verbesserungen und Funktionen bereitzustellen.
              link:
                href: https://docs.gitlab.com/ee/user/analytics/index.html
                text: Mehr erfahren
                ga_name: Analyze GitLab usage
                ga_location: body
next_steps:
  header: Bringen Sie Ihr Unternehmen voran
  cards:
    - title: Haben Sie einen kostenpflichtigen Tarif?
      text: Dann steht Ihnen ein Technical Account Manager (TAM) zur Seite.
      avatar: /nuxt-images/icons/avatar_orange.png
      col_size: 4
      link:
        text: Mein TAM soll Kontakt mit mir aufnehmen
        url: /sales/
        data_ga_name: Have my TAM contact me
        data_ga_location: body
    - title: Benötigen Sie weitere Hilfe?
      text: GitLab Professional Services hilft Ihnen bei den ersten Schritten, der Integration mit Anwendungen von Drittanbietern und vielem mehr.
      avatar: /nuxt-images/icons/avatar_pink.png
      col_size: 4
      link:
        text: Mein PS soll Kontakt mit mir aufnehmen
        url: /sales/
        data_ga_name: Have my PS contact me
        data_ga_location: body
    - title: Bevorzugen Sie die Zusammenarbeit mit einem Vertriebspartner?
      avatar: /nuxt-images/icons/avatar_blue.png
      col_size: 4
      link:
        text: Vertriebspartnerverzeichnis ansehen
        url: /sales/
        data_ga_name: See channel partner directory
        data_ga_location: body
    - title: Erwägen Sie ein Upgrade?
      text: Lesen Sie mehr über die Vorteile der Tarife [Premium](/pricing/premium/){data-ga-name="why premium" data-ga-location="body"} und [Ultimate](/pricing/ultimate/){data-ga-name="why ultimate" data-ga-location="body"}.
      col_size: 6
      link:
        text: Tarifdetails ansehen
        url: /sales/
        data_ga_name: See tiering details
        data_ga_location: body
    - title: Erwägen Sie die Integration eines Drittanbieters?
      text: GitLab Professional Services hilft Ihnen bei den ersten Schritten, der Integration mit Anwendungen von Drittanbietern und vielem mehr.
      col_size: 6
      link:
        text: Unsere Allianz- und Technologiepartner anzeigen
        url: /partners/
        data_ga_name: See our Alliance and Technology partners
        data_ga_location: body
