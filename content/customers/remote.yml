---
  data:
    title: Remote
    description: With just one year under its belt, Remote is improving global employment with GitLab SCM and CI/CD.
    og_title: Remote
    twitter_description: With just one year under its belt, Remote is improving global employment with GitLab SCM and CI/CD.
    og_description: With just one year under its belt, Remote is improving global employment with GitLab SCM and CI/CD.
    og_image: /nuxt-images/blogimages/remote.jpg
    twitter_image: /nuxt-images/blogimages/remote.jpg
    customer: Remote
    customer_logo: /nuxt-images/logos/remote-logo.svg
    heading: How Remote meets 100% of deadlines with GitLab
    key_benefits:
      - label: Quick iterations
        icon: speed-alt
      - label: Zero context switching
        icon: agile
      - label: Single source of truth
        icon: continuous-delivery
    header_image: /nuxt-images/blogimages/remote.jpg
    customer_industry: Technology
    customer_employee_count: 10
    customer_location: Remote
    customer_solution: |
      [GitLab Silver](/pricing/){data-ga-name="silver solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: Deadlines met
        stat: 100%
      - label: Focus on product
        stat: 100%
      - label: Code pushed in the last three months
        stat: 3,795
    blurb: Remote uses GitLab as a single source of truth, iterating fast from ideation to delivery.
    introduction: |
        With just one year under its belt, Remote is improving global employment with GitLab SCM and CI/CD.
    quotes:
      - text: |
          Within a year, we never had a case of not meeting a deadline because we had to spend time working on the CI pipeline or something related to the tooling. That has never been the case.
        author: Marcelo Lebre
        author_role: Co-founder And CTO
        author_company: Remote
    content:
      - title: Solving global employment
        description: |
          Remote is a global organization that provides a platform to employ anyone anywhere in the world. The company started just one year ago completely from scratch with the goal to reform the way global employment works. [Remote](https://remote.com/) helps to place employees with full-time working roles, as opposed to contract or freelance positions like most remote opportunities. It creates a solution to employ people in different countries, acting as a global employer of records.
      - title: Avoiding multi-toolchains and unnecessary costs
        description: |
          A startup is a challenge in and of itself, but being a startup with the premise of global organizational employment is an even bigger ambition. The company is dependent upon productivity and they wanted a tool that would provide operational efficiency and enhanced product delivery. Remote is a lean team and when they expand, they’ll need a tool that will scale with them.

          Because Remote depends on communication worldwide, the development team needed a tool for source code management and continuous integration. “There is one underlying need or requirement that I have for the projects that I manage and Remote isn’t an exception. It is that from the ideation step to the delivery, it needs to be as smooth as possible and as fast as possible,” said Marcelo Lebre, co-founder and CTO of Remote. “Any deviation to this sort of stream by any measurement, even if it’s very small, is very costly to the whole company and to the people themselves because it translates into waste and waste is inefficiency.”

          Lebre and his team have all had previous experience working with multi-toolchains and understand the added amount of time, cost, and work needed. With multiple tools, smaller startups usually have to manually code, test, and deploy or wire it all together explicitly. A developer’s time would be spent configuring and managing the various tools. If one tool breaks, it negatively impacts the whole system, sidetracking the engineering team.
      - title: Building a startup with GitLab
        description: |
          Building [speed with a startup](https://about.gitlab.com/solutions/startups/) usually requires a variety of software tools. According to Lebre, “Every small startup had to use a plethora of tools. They had to use things like Codeship, Trello, Basecamp, Asana, or Jira … We’ve used them all together to make sure that you could ship something, and iteratively, because otherwise what I saw in smaller startups was that they would have to do all the things by hand.”

          Members of the Remote team had previously used GitLab and came to the conclusion to use the platform once again fairly quickly. “To be honest, when we started Remote it was already a no-brainer. I have been using GitLab for many years already. So I mean, there’s no competition there,” Lebre said. The threshold to get started with other tools was much higher because it meant picking out individual tools for individual services. Since the team was comfortable with GitLab, it helped get the start-up moving faster than had they chosen another platform.
      - title: Operational efficiency, on-time deliveries, and zero maintenance
        description: |
          From the very inception, Remote has used GitLab. The entire small company is using the platform, both developers and non-developers, with the intention to expand and maintain GitLab as the infrastructure. For now, Remote has one software in one location and focuses on quick iterations.

          The issues used in GitLab are the single source of truth and because team members are all remote, this keeps everyone in the loop. Almost zero time is spent managing the tool with the ability to link directly between the issues to code and the pipeline allows a continuous visibility and workflow. “GitLab has made it easier to be a remote company because we document everything and make sure all our code and product is visible in GitLab,” Lebre said. “Through GitLab, we have full observability over our delivery speed and iteration process so that we can optimize where we need to.”

          The development team has eliminated the [need for a multi-toolchain](https://about.gitlab.com/customers/knowbe4/) by using GitLab for SCM and CI/CD. “We pride ourselves for not making people overwork. Engineering is a craft, I believe in that, and making people work overtime reduces the quality of that craft,” Lebre said. “If I use four tools to do the same as I do with GitLab, it means that the team is spending time managing those tools and jumping off and on from those tools. So either we work more hours, or we ship less. Those two options are not something that I looked forward to as a manager.”

          Remote developers spend 100% of their time working directly on the product. Lebre and his team appreciate the transparent end-to-end platform, negating any possibility of being blindsided by an issue, which has allowed them to meet deadlines 100% of the time. In the last three months, the team has shipped over 540 merges to production and engineers have updated code 3,795 times. “I can say that GitLab and the full suite has been an enabler, and never a problem we had to fix,” Lebre said.
