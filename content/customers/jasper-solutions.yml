---
  data:
    title: Jasper Solutions
    description: How Jasper Solutions offers “DevSecOps in a box” with GitLab
    og_title: Jasper Solutions
    twitter_description: How Jasper Solutions offers “DevSecOps in a box” with GitLab
    og_description: How Jasper Solutions offers “DevSecOps in a box” with GitLab
    og_image: /nuxt-images/blogimages/jasper.jpg
    twitter_image: /nuxt-images/blogimages/jasper.jpg

    customer: Jasper Solutions
    customer_logo: /nuxt-images/logos/jasper_logo_color.svg
    heading: How Jasper Solutions offers “DevSecOps in a box” with GitLab
    key_benefits:
      - label: Higher customer satisfaction
        icon: access
      - label: ROI and RTO increased
        icon: piggy-bank-alt
      - label: Hundreds of work hours saved annually
        icon: devsecops
    header_image: /nuxt-images/blogimages/jasper.jpg
    customer_industry: Technology
    customer_employee_count: 87
    customer_location: NY, VA, HI, GA, IA
    customer_solution: |
      [GitLab Ultimate](/pricing/ultimate/){data-ga-name="ultimate solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: Reduction in cycle time
        stat: 30%
      - label: Increase in deployment frequency
        stat: 25%
      - label: Of projects on budget and on time
        stat: 90%-95%
    blurb: Jasper builds client-specific solutions using GitLab SCM, CI, and CD
    introduction: |
      With GitLab as their single stack software for CI, CD, and SCM, Jasper teams no longer worry about pipeline failures
    quotes:
      - text: |
          If a certain feature is not available, the ability to have a discussion with GitLab in the open and to put it on a road map so we can tell a customer that you can start using the existing platform and the features and functionality and within three months we will offer this specific feature to acknowledge your need. There is no other single product in the industry that does that.
        author: Andy Patel
        author_role: CEO and Owner
        author_company: Jasper Solutions
    content:
      - title: Empowering the tactical edge for the public sector
        description: |
          [Jasper Solutions](https://www.jaspersolutions.com/) is a software development company that provides a broad range of business and technology services. Jasper offers clients worldwide access to enhanced workflow performance with the goal of aligning IT objectives with business objectives. The diverse customer range includes federal, state, and local government agencies, as well as several commercial enterprises. Jasper specializes in advanced analytics for data management, governing public or private cloud computing, and cyber security.

          One of the leading solutions that Jasper provides enables organizations with tactical edge. “Tactical edge by definition is any individual or any operation that’s a field operation where no network connectivity or any network intermittent connectivity is provided, and primarily the field operations can consist of anything from logistics, to cyber, to information gathering, to an ISR capability,” according to [Andy Patel](https://www.linkedin.com/in/anshuman-andy-patel-35a48a/), CEO and Owner of Jasper Solutions.

          Jasper Solutions offers constantly updated evolutionary data in the form of “DevSecOps in a box,” which enables operations at the tactical edge and gives the ability to modify any application to fit an environment and update intermittently when a network is established. Public sector clients, including war fighters, airmen, Marines, sailors, and other sectors of military service rely heavily on this service. “The ability to have the most recent updated information at the tactical edge can save a life or cause a loss of battalion,” Patel said.
      - title: Too many tools causing too many failures
        description: |
          Over the past five years, Jasper has been working with traditional pipelines, which included tools from Atlassian, BitBucket, Jira, Jenkins, and SonarQube. Developers provided a variety of pipelines to Jasper customers to enable their development. Jasper kept running into challenges with broken pipelines caused by single tool updates. Each time an upgrade was available for one tool, feature, or function, the pipeline would break and the development team would spend more time fixing the pipeline than actually developing code for customers.

          “We used to use a lot of tools and the biggest challenge that we had was that we were more worried about when the pipeline was going to break and how to actually fix the pipeline or roll it back so we could continue to do development and then introduce the additional features and functionalities by each different product in order to get it fully configured and fully tested,” Patel said. “A lot of times the development and test environment would have various different test packages with different vendors.”

          Jasper’s teams spent years working with variations of different tools to develop their products to empower customers to work at the tactical edge and each one of them failed or didn’t meet requirements. According to Patel, nine out of 10 times the field operations did not have the latest and greatest tools to address their needs. “For a long, long time we could not put a solution together simply because we did not find tools that would work to fit specific customer requirements,” Patel said.

          In order to develop “DevSecOps in a box,” Jasper needed a platform that deploys easily, offers code management, has security awareness, and ideally, is all in one tool. A single platform would allow the team to [focus on development](gitlab.com/stages-devops-lifecycle/), rather than on constant pipeline failures caused by updates from multiple tools. Not only would this capability help team members, it would also improve customer satisfaction with better products delivered at a faster rate.
      - title: Transparency provides competitive edge over 260 tools
        description: |
          Jasper adopted GitLab after testing more than 260 other tools. Jasper teams have been using GitLab for the past three and a half years. For the first two years, GitLab was only used internally for development. GitLab became part of the external service after the team saw how easily GitLab enabled deployments. “Knowing and understanding GitLab’s capability, the feature and functionality, the constant updates to add additional feature functionality and the ease of usability by end users to work the pipeline really enabled my internal team at Jasper to use the GitLab pipeline to build our internal development, maintain our applications, and now build our customer applications as well,” Patel said.

          According to Patel, GitLab’s transparency of the platform set it apart from the competition. Not only is the usability of tools transparent, but GitLab’s roadmap is transparent. “From a value proposition perspective, especially in the [DevSecOps](/solutions/dev-sec-ops/){data-ga-name="devops" data-ga-location="body"} space where it’s ever evolving and constantly changing, the ability to discuss a specific problem or a gap that GitLab currently does not offer and actually put it on a road map, no other company does that while being completely transparent. And, at the same time, addressing the entire stack of CI, CD and SCM,” Patel said.

          GitLab offers every feature, functionality, and benefit that the customers can take advantage of. GitLab is now their single stack software for CI, CD, and SCM. The pipeline gets updated from one end to the other, so teams no longer worry about pipeline failures. “With GitLab, you have the entire pipeline as a single track software to address all of the CI, CD, SCM functionality. So when we do one single upgrade, the entire pipeline gets upgraded with all of the new features and functionalities and 99.9% of the time we’re not breaking the pipeline and we can still address customers’ software development needs and focus on the application lifecycle rather than worry about our pipeline,” Patel said.
      - title: GitLab is the default for “DevSecOps in a box”
        description: |
          Jasper has reduced cycle time by 30% since introducing GitLab more than three years ago. Deployment frequency has accelerated by 25% because the pipelines are not breaking. “The ability to deliver our products with constant updates certainly increases. The ROI and the RTO times have increased, and the value proposition for the last three periods, year-over-year, have certainly increased because we’re providing a better value proposition from delivery, from execution, and also from a management perspective,” Patel said.

          GitLab is used to build internal and external products, so the pipeline remains the same. Ninety percent to 95% of project releases are on budget and on time. Some features and functionalities may change depending on a specific need, but between the internals of the organization, as well as the customers, GitLab is the solution. “Having a pipeline that doesn’t break, that’s roughly 350 man hours a year that we save on average for each individual product of ours, if not more,” according to Patel.

          For the past year and a half, Jasper has been building solutions for customers using GitLab and in early 2020, Jasper became a GitLab partner. By December 2019, Jasper had finished the prototype architecture for “DevSecOps in a box” and was rolling it out to customers. “GitLab is by default on every box that we deploy. It is offered as a base solution. The rest of the components and open source software are all complimentary to fit around the DevSecOps scenario in the box,” Patel said. Each customer, from Marine to airman, has specific requirements, so GitLab is the base solution and additional capabilities are added to the overall toolkit accordingly. The “DevSecOps in a box” enables [multicloud deployments](/topics/multicloud/){data-ga-name="multicloud" data-ga-location="body"} for customers, including Azure, AWS, Google, IBM and Oracle Cloud.

          Jasper is saving between 33%-37% annually since adopting GitLab for SCM, CI, and CD. “If we were purchasing individual tools, plus maintenance and upgrades on those specific tools, and add each one of them up individually and do a price comparison side-by-side to GitLab and if you do a ROI model, the amount of money that we save is tremendous,” Patel added.

          Since rolling out “DevSecOps in a box,” customers have been eager to test it out. “We have roughly 35 customers who have been currently working to take a delivery. We have four successful customers who already have it and they cannot stop raving about it. We are working on roughly additional 22 to 23 customers who have heard about it or have seen the capabilities and are interested,” according to Patel.
