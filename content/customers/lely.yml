---
  title: Lely
  description: Lely adopted GitLab for improved collaboration with built-in code reviews and continuous integration.
  image_title: /nuxt-images/blogimages/lelyservicesoption2.jpg
  image_alt: 
  twitter_image: /nuxt-images/blogimages/lelyservicesoption2.jpg
  data:
    customer: Lely
    customer_logo: /nuxt-images/customers/lely_logo.svg
    heading: How Lely replaced three tools with GitLab to maximize efficiency
    key_benefits:
      - label: Fewer tools to maintain
        icon: cog
      - label: Easy adoption
        icon: code
      - label: Improved collaboration
        icon: collaboration
    header_image: /nuxt-images/blogimages/lelyservicesoption2.jpg
    customer_industry: Technology
    customer_employee_count: 1,200
    customer_location: The Netherlands
    customer_solution: |
      [GitLab Self Managed Premium](/pricing/premium/){data-ga-name="premium solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: tools replaced by GitLab
        stat: '3'
      - label: groups using GitLab
        stat: '171'
      - label: projects
        stat: '1,278'
    blurb: Lely, a Dutch robotics manufacturer specializing in dairy farm automation, uses GitLab for better collaboration and less toolchain maintenance.
    introduction: |
      Lely adopted GitLab for improved collaboration with built-in code reviews and continuous integration.
    quotes:
      - text: |
          For a software engineer, everything is there, so the developers feel more empowered. All their work information is in one overview.
        author: Kees Valkhof
        author_role: Configuration Manager
        author_company: Lely
    content:
      - title: An innovator in agricultural robotics
        description: |
          Since 1948, Lely has been an international family business in the agricultural sector. [Lely](https://www.lely.com/us/about-lely/){data-ga-name="lely" data-ga-location="body"} specializes in robotic equipment and innovative solutions for dairy farmers in more than 45 countries. The company offers dairy automation to make farmers’ lives easier and help farms operate more efficiently. Lely’s services include robots that milk and feed, and stable cleaners that can operate independently. Lely’s inventions and automated systems produce healthier and more sustainable farms.

      - title: Adopt Git and do more with fewer tools
        description: |
          The development team at Lely was using Subversion (SVN) for [version control](/topics/version-control/){data-ga-name="version control" data-ga-location="customers content"}. However, the team was looking to move to Git because it is “easier to use Git correctly,” according to Kees Valkhof, configuration manager at Lely. The SVN repository was messy and lacked transparency, which created confusion around where data was stored.

          The development team had to manually write scripts to ensure that reviews worked correctly and in order for engineers to do merge requests correctly in SVN. Developers were also using Review Board for code reviews, which required maintenance in order to continually integrate it with SVN. Having to open up a separate tool to perform code reviews meant that sometimes new code was getting deployed without ever being properly reviewed. With developers using multiple tools, there was little visibility into the status of projects. The maintenance and integration required to keep the developers up and running was taking up too much time and slowed throughput.

          Lely developers were looking for a modern solution to replace their legacy tooling, including their homegrown SVN server, TeamCity, and Review Boards. Ideally, the solution would be an integrated, automated, singular platform that the developers could adopt quickly. The development team wanted to continue to innovate and consolidate the toolchain to become more productive. “We want to use as few tools as possible, and we want to stay on the same tool that everyone can use,” Valkhof said.

          By replacing these older tools, the Lely team hoped they could continue to innovate internally, just as they innovate for their clients. The team had three requirements for any new tools:
            
            1. **A Git workflow**: With SVN being outdated and unable to do what teams needed, Git adoption would be a top priority.
            
            2. **Simple, all-in-one solution**: To reduce maintenance, teams wanted one solution that could do multiple tasks and integrate with Grafana dashboards.
            
            3. **Easy adoption**: The tool(s) would need to be intuitive with a low barrier to entry for all team members to adopt quickly and efficiently.

      - title: Developers collaborating in one tool 
        description: |
          A Lely development team working in the Czech Republic had installed GitLab via open source and was using it internally. GitLab received very positive feedback from the Czech team’s experience. Valkhof and his team reviewed GitLab and decided to purchase Premium in order to roll it out to the rest of the development team. “If you already have a working system, then I don’t have to figure out how it works,” Valkhof said.

          [GitLab’s built-in continuous integration](/solutions/continuous-integration/){data-ga-name="continuous integration" data-ga-location="customers content"} and code reviews gave developers greater visibility. “What I liked in GitLab when I started using it is the build system is integrated and the review system is integrated,” Valkhof said. “We don’t have to log with a whole other system.”

          GitLab’s complete DevOps platform, delivered as a single application, means developers spend less time managing tools and won’t have to cobble together Python code just to visualize data. With the Grafana dashboard and GitLab integration, teams know exactly what’s in the workflow process.


      - title: A unified DevOps workflow with less maintenance
        description: |
          Lely’s operations team is now managing fewer tools. “My maintenance became easy because I had to maintain fewer servers. And I didn’t have to maintain the connections,” Valkhof said. GitLab helps support collaboration within Lely services and between management and development. “[Collaboration] is better than what we had with Subversion. Now it’s much easier to review changes because the button to click and the information that we have is for an entire process or standard boards,” Valkhof said. “We can involve the managers with our process that we defined ourselves.”

          GitLab’s overview pages show test schedules, confidence results, and competency fields. This gives developers the visibility into the status of a project and, moreover, how if that project status directly impacts their workflow. “Before that, we never had good insights into the impact. The overview page of the emergent list contains usual information and we didn’t have that. So it’s basically a dashboard full of useful information for the developer, which is helpful,” Valkhof added.

          GitLab’s single application makes it easier to onboard new developers because Valkhof doesn’t have to train them on multiple tools. GitLab is designed to be very intuitive, so onboarding happens a lot faster. “They almost immediately know how to use it. And the few that don’t, I can help in just minutes,” Valkhof added. GitLab helped the team move to Git, adopt better practices around code review and testing, and empowered developers. “GitLab has allowed us to do more complex projects while simplifying how we visualize and track progress,” Valkhof said.
    customer_success_stories:
      title: Customer success stories
      link:
        text: See all stories
        href: /customers/
        ga_name: all customers
        ga_location: customers stories
      read_story: Read story
      stories:
        - title: Goldman Sachs
          image: /nuxt-images/customers/goldman_sachs_case_study.png
          description: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
          link: /customers/goldman-sachs/
          ga_location: customers stories
        - title: Siemens
          image: /nuxt-images/customers/siemens.png
          description: How Siemens created an open source DevOps culture with GitLab
          link: /customers/siemens/
          ga_location: customers stories
        - title: Fanatics
          image: /nuxt-images/customers/fanatics_case_study.png
          description: Fanatics' successful GitLab CI transition empowers innovation cycles and speed
          link: /customers/fanatics/
          ga_location: customers stories
