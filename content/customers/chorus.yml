---
  data:
    title: Building the future of conversation intelligence
    description: GitLab’s single tool for the software development lifecycle helps Chorus.ai capture the power within sales calls
    og_title: The Future is Conversation Intelligence
    twitter_description: GitLab’s single tool for the software development lifecycle helps Chorus.ai capture the power within sales calls
    og_description: GitLab’s single tool for the software development lifecycle helps Chorus.ai capture the power within sales calls
    og_image: /nuxt-images/blogimages/Chorus_case_study.png
    twitter_image: /nuxt-images/blogimages/Chorus_case_study.png

    customer: Chorus
    customer_logo: /nuxt-images/logos/chorus-logo.png
    heading: Building the future of conversation intelligence
    key_benefits:
      - label: Enhanced collaboration
        icon: collaboration
      - label: Improved testing
        icon: monitor-test
      - label: Faster production cycles
        icon: accelerate
    header_image: /nuxt-images/blogimages/Chorus_case_study.png
    customer_industry: Technology
    customer_employee_count: 110
    customer_location: San Francisco, Tel Aviv, and Boston
    customer_solution: |
      [GitLab Ultimate](/pricing/ultimate/){data-ga-name="ultimate solution" data-ga-location="customers hero"}
    blurb: Chorus.ai offers sales teams an easy way to capture and summarize call notes so they can focus on really listening to customers.
    introduction: |
      GitLab’s single tool for the software development lifecycle helps Chorus.ai capture the power within sales calls.
    quotes:
      - text: |
          One tool just makes life easier. It's just less stuff to manage. When you use too many tools you end up having one thing over here and other items over there. This leads to struggles around keeping tasks together and keeping everything up to date.
        author: Russell Levy
        author_role: Co-founder and Chief Technology Officer
        author_company: Chorus.ai
      - text: |
          We wanted something that was integrated and fully thought through. GitLab actually thinks about every feature built and how it fits in with the rest of the platform. When you see how everything is very well-connected in the system, it’s really, really powerful.
        author: Russell Levy
        author_role: Co-founder and Chief Technology Officer
        author_company: Chorus.ai
      - text: |
          It's amazing what you guys have done with Auto DevOps with just a few lines in a GitLab CI file. It has really helped us to shorten lead time, which has positively affected every single metric we measure. It means that we're having smaller pushes to production and are able to do more of them. We're having fewer failures in production because we have ways to test the application.
        author: Russell Levy
        author_role: Co-founder and Chief Technology Officer
        author_company: Chorus.ai
    content:
      - title: From dream to reality
        description: |
          Chorus.ai was built out of the fire of three dreamers who were passionate about forming a new company. After watching the market and seeing the rise of smart home gadgets and new machine learning capabilities, the trio decided to build software around analyzing conversations. Focusing on conversations around sales, Chorus.ai enables account executives to focus on what is actually being said and respond appropriately — instead of taking notes.

          They developed Chorus.ai as a conversation intelligence platform. It records, transcribes, and analyzes customer conversations to help sales teams and support teams. The platform offers transcription and AI-based analytics as well as sharable snippets to easily share conversational insights with other team members.
      - title: Creating a bold new development process
        description: |
          How do you design a build process from scratch? For Chorus, the founders relied on previous knowledge and experience to decide on a single development tool. They decided they didn’t want a toolchain comprised of disparate tools and plugins, so they searched for a DevOps solution to meet their needs.

          With GitLab, the teams are able to focus on development instead of processes and systems. Using GitLab has also increased the collaboration between teams. “Life is just so much easier for product engineering and anyone who wants to interact with product engineering because they can just do it through GitLab,” Levy explained.
      - title: A single tool to integrate development teams
        description: |
          Chorus.ai selected GitLab because the team was impressed by the integration the application offers. Levy says that GitLab is the only product on the market that offered a true integration and made every process easy.

          “The only other company that has anything that’s close to GitLab in our view was Atlassian, and they may offer all those tools under the Atlassian company, but they’re not integrated. So you buy Bitbucket, and it somehow ties up with this one. Then you have to deal with pricing on each one, users for each one … the whole thing, it’s just a bunch of products that are stitched together,” Levy explains.
            
          During a recent audit for SOC2 compliance, the auditors said that Chorus had the fastest auditing process they have seen and most of that is due to the capabilities of GitLab. Levy explained that it was easy to write a script and get the information from the API to make the audit easier.
      - title: Speed, security, and Auto DevOps
        description: |
          Because Chorus was developed with GitLab from an early stage, they conquered common build challenges that many companies experience. They made the conscious decision to keep everyone collaborating with a single tool to avoid the use of plugins and disparate tools. Within GitLab, Chorus is able to increase development velocity by beginning sprints in GitLab as epics. Teams use issues and boards to improve their capacity planning. For releases, teams run their pipelines using GitLab CI/CD.

          Chorus has also shortened its feedback loops by using the SAST and DAST capabilities within GitLab. “We started using the GitLab security dashboard a month ago. It has been very beneficial to see if there are new security issues that are coming up. It really helps the developers. We can now show them why their code and truly show them why this is an issue, and how it could be exploited,” Levy said.

          The team at Chorus also credits GitLab for helping them improve their feature cycle analytics. By having test results, security reviews, performance tests, the code climate and everything in the merge requests, Chorus has been able to move quickly. Chorus is also utilizing the Auto DevOps capabilities of GitLab to deploy to their AWS S3 servers. GitLab Auto DevOps automates CI/CD configuration to simplify the execution of their development.



