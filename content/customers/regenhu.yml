---
  data:
    customer: REGENHU
    customer_logo: /nuxt-images/case-study-logos/regenhu.png
    heading: How REGENHU reaps 50% YoY in cost savings annually with GitLab
    key_benefits:
      - label: Improved operational efficiency
        icon: accelerate
      - label: Cost savings
        icon: piggy-bank-alt
      - label: Easy onboarding
        icon: cog-user-alt
    header_image: /nuxt-images/blogimages/cover_image_regenhu.jpg
    customer_industry: Technology
    customer_employee_count: 33
    customer_location: Switzerland
    customer_solution: |
      [GitLab Self Managed Premium](/pricing/premium/){data-ga-name="premium solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: cost savings year over year
        stat: 50%
      - label: less time spent on build, test, and deploy work
        stat: 80%
      - label: fewer bugs
        stat: 40%
        
    blurb: REGENHU adopted GitLab to empower developers to create and collaborate on code in a single transparent, robust solution.
    introduction: |
      REGENHU adopted GitLab for version control, improved collaboration, and CI/CD automation.
    quotes:
      - text: We have two big things thanks to GitLab. First, on the management and company side, we really had some nice economic results with GitLab. And on the development side and operational side, GitLab facilitates the team to really help bring the agile mindset to the company.
        author: Samuel Gilliéron
        author_role: Software Architect, REGENHU
        author_company: REGENHU
    content:
      - title: 'Cutting-edge bioprinting solution'
        description: |
          [REGENHU](https://www.regenhu.com/) was established in 2007 and is a pioneer in converging digital manufacturing, biomaterials, and biotechnology to lead transformational innovations in healthcare. Based in Switzerland, REGENHU assists the scientific community with cutting-edge 3D bioprinting solutions.
      
      - title: Lacking efficiency and transparency
        description: |
          Before finding their way to GitLab, [REGENHU](https://www.regenhu.com/) had no real agile methodologies in place. The teams were using Jira to list backlogs in place for smaller teams, but the processes were not able to expand and mature quickly enough for a growing enterprise. REGENHU developers decided Jira was too complex of a workload without any added value to the company. “We tried first to implement some workflow on Jira, but it quickly became heavy work for not a lot of value for us. We wanted to have something which was pretty straight forward,” said Samuel Gilliéron, software architect. REGENHU teams were working on a local server using Word documents and PDFs for documentation and software projects. “It was quite easily a nightmare to manage all of this,” Gilliéron said.

          The goal was to find a solution to adopt that provides a single repository for both source and configuration documentation. Ideally, the platform would strengthen code collaboration and be transparent for both developers and those outside of the development team. Visibility would provide all stakeholders with a broad understanding of the workflow status. REGENHU also hopes to expand the organization. In order to do so, REGENHU needed a solution that is cost effective, offers CI/CD capabilities, and provides cross-collaboration in one place, all while having the ability to support and grow with the company. “We don't want to have a lot of audits. We don't want to have a lot of overhead. So we wanted to have something which is both containing everything or as much as we can while still being quite light in usage,” according to Gilliéron.
      
      - title: One platform to grow with 
        description: |
          The development team reviewed GitHub for version control, Jenkins for CI/CD, and Microsoft VSTS for delivery. “We saw Jenkins for CI/CD, but the overhead of maintaining everything and bringing onboard everything was quite intensive ... Microsoft is great if you work purely with .NET and so on, but we also wanted to have other people in the company who are more on the Python side to also be quickly integrated,” Gilliéron said. “We wanted something which was software agnostic, and that's why we chose GitLab.”
      
          The other tools they reviewed didn’t offer an all-in-one solution, which meant that developers would need more than one tool for code management and CI/CD. Between time management and cost effectiveness, this wasn’t an option due to the additional time and cost of maintaining the overhead of a multi-vendor solution. 
        
          “GitLab is such a reasonable price. We were able to grow the team to mature,” Gilliéron said. “We also were able to convert some of the other people in the company to become aware of more modern software processes by bringing them into GitLab.” On top of that, GitLab integrates with tools that REGENHU has in place, including SonarQube and TestRail. REGENHU currently manages everything on-premises, but GitLab’s flexibility allows them to consider moving to the cloud.
      
      - title: Version control, CI/CD, and 50% YoY cost savings
        description: |
          Developers started using milestones as their sprints, created backlogs, and used lists and boards for effective project management. Teams were eager to get on board with the new agility-focused mindset of the company. Soon thereafter, the focus turned to implementing CI/CD. They initially started building applications, then began adding automation of unit testing and code testing. “Time spent in build, test, and deploy decreased by 70-80%. Before GitLab, we had to do it on our machine which of course never worked,” Gilliéron said. Developers now have the ability to own their own code and see that it is done correctly.
    
          Code quality increased greatly, and time to implement new features has decreased by 40-60% on average. Code coverage has increased from 10-20% to now between 50-60% with the introduction of GitLab’s ability to cover new code. On top of that, spontaneous bug discoveries have decreased 30-40% overall. “It’s pretty much doubled the efficiency of each developer because we are doing an internal release every month. Each month everyone in the company says, ‘Oh, we are going very fast,’” Gilliéron said. “It's quite impressive, the amount of changes and improvements that you do. [GitLab] has given a lot of power to each of our developers.”  
      
          Not only has efficiency doubled, but cost savings is estimated at about 50% year over year. “Even accounting for the cost of hosting this on our server, we are still way below. I think it is 50% cheaper with GitLab per year. So, it is quite quickly delivering savings,” Gilliéron said. GitLab users are now seeing the benefit of having integrated source control and CI, and are able to quickly understand when things aren’t working. “Once we first do the first push, then it's pretty much working by itself. We really think that GitLab is really helping users because things are quite clear and quite communicative,” Gilliéron added.    
      
          REGENHU now prides themselves on their improved documentation, collaboration, and source control. The team has completely moved to a mono repository, with documentation, source code, and configuration in a single platform. CI/CD alleviates management issues and developers have more time to focus on production deployments.
       
