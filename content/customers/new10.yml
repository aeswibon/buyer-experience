---
  title: New10
  description: How New10 deploys 3 times faster with GitLab
  image_title: /nuxt-images/blogimages/new-ten-euro.jpg
  image_alt: 
  twitter_image: /nuxt-images/blogimages/new-ten-euro.jpg
  data:
    customer: New10
    customer_logo: /nuxt-images/logos/new10-green-logo.svg
    customer_logo_full: true
    heading: How New10 deploys 3 times faster with GitLab
    key_benefits:
      - label: Improved efficiency
        icon: accelerate
      - label: Accelerated releases
        icon: speed-alt-2
      - label: Built-in security scanning
        icon: devsecops
    header_image: /nuxt-images/blogimages/new-ten-euro.jpg
    customer_industry: Financial Services
    customer_employee_count: 100
    customer_location: Netherlands
    customer_solution: |
      [GitLab Gold](/pricing/){data-ga-name="gold solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: faster deployments
        stat: 3x
      - label: context switching
        stat: 0
      - label: shorter cycle times
        stat: 20x
    blurb: New10 wanted a solution that could provide version control and code ownership for engineers to deploy quickly and safely.
    introduction: |
      New10 found startup success using GitLab as a single platform for source code management (SCM) and continuous integration and delivery (CI/CD), and GitLab's Amazon Web Services (AWS) integration.
    quotes:
      - text: |
          GitLab is really helping us in our very modern architecture, because you're supporting Kubernetes, you're supporting serverless, and you are supporting cool security stuff, like DAST and SAST. GitLab is enabling us to have a really cutting edge architecture.
        author: Kirill Kolyaskin
        author_role: CTO
        author_company: New10
    content:
      - title: Growing FinTech start-up
        description: |
          New10 is a digital financial lender based in the Netherlands offering loans to small and medium-sized enterprises. [New10](https://new10.com/) is 100% a subsidiary of ABN AMRO Bank N.V., combining the speed and innovation capacity of a startup with the expertise and security of a bank. The FinTech startup helps entrepreneurs to accelerate business growth with fully digital financial services.

          The whole lending process, from application phase to officially signing, happens within 15 minutes. Entrepreneurs can secure a financing offer with clear terms and conditions via a professional and efficient digital process. In just three years, New10 has provided loans to more than 2,500 happy customers with an average customer satisfaction score of nine.
      - title: Getting started swiftly and securely
        description: |
          Three years ago, New10 was established by a small group of highly motivated professionals, most of whom had backgrounds in engineering with banks or other financial organizations. The engineers had a variety of software platform knowledge, but none had previously worked with GitLab. From the inception of the company, the engineers wanted a solution that was developer-friendly and scalable. Ideally, the solution would allow developers to own their own code, creating quick deployment turnaround times.

          As a startup, it was crucial for the company to bring value to enterprises as soon as possible while maintaining high standards. The main requirement was to have Git functionality integrated together with CI/CD, essentially avoiding context switching to increase workflow efficiency. New10’s affiliation with ABN AMRO Bank requires strong security and privacy regulation compliance. The company has always placed protection at the forefront and a solution that focuses on high-level security would offer another level of assurance.
      - title: All-in-one workflow efficiency
        description: |
          New10’s engineers had experience with several other platforms, but they adopted GitLab from inception in order to get up and running quickly. GitLab’s all-in-one solution allows developers to work efficiently without any context switching, unlike other tools.

          “If you have to go to your data center, you have to buy servers, the servers have to be shipped, you install some kinds of Jenkins on the servers, you configure it, and then you can start developing, only then can you start to produce. With GitLab, you really cut the time in configuration, cut the time to what is crucial for you to deliver,” said Kirill Kolyaskin, CTO.

          New10’s engineers own their own code with GitLab’s [version control system](https://about.gitlab.com/topics/version-control/) and CI/CD. “It’s not only Git and CICD, Infrastructure as a code and application security is also owned by the developer. It’s really bringing ownership and power into the hands of people who write the code. This is the main competitive advantage and deal breaker when I’m using GitLab. No one else does it in the market,” according to Kolyaskin.
      - title: Fast deployments, increased releases, supported engineers
        description: |
          With CI/CD properly in place and incorporating GitLab’s best practices, changes are made directly to the code in real time. The deployment is altered immediately, allowing developers to deploy three times faster than they previously were able to. Having an enhanced and integrated source control management tool has improved efficiencies amongst developers. The solution provides the opportunity to work asynchronously, and integrates with several other tools.

          GitLab integrates with Slack, which allows notifications and improves workflow. New10 also plugs GitLab into its data analytics tool, Tableau. This integration allows data from GitLab to give visibility of the business acceleration to stakeholders. New10’s engineers are thriving in a DevOps culture. The engineers are able to release new features and new products because they no longer need to maintain deployments. The deployment to production release time went from two times per month to two times per day, with GitLab directly supporting this success.

          GitLab closely [integrates with AWS](https://about.gitlab.com/partners/technology-partners/aws/) and this integration has delivered great value for New10. They run all their production workloads in AWS, including loan administration, risk calculation, customer information and analytical services. “It’s a great cloud native integration for us,” added Kolyaskin. New10 uses GitLab runners for testing and quality assurance purposes, and these runners are hosted on Kubernetes clusters. Their engineers are empowered to run static and dynamic tests on their code. The data is provided to a developer during the merge request, saving team members from security checks in various departments and then looping back to developers. “GitLab helped us in our initiative to kind of shift security left. We have a shorter feedback loop on security and we can fix problems in code before they are even deployed,” Kolyaskin said.

          New10’s level of security has always been “like Fort Knox,” according to Kolyaskin. With GitLab, teams are able to maintain a high level of security faster and at an inexpensive rate. “GitLab is really helping us in our very modern architecture, because you’re supporting Kubernetes, you’re supporting serverless, and you are supporting cool security stuff, like DAST and SAST. GitLab is enabling us to have a really cutting edge architecture.”

          GitLab’s technical account managers are engineers who understand and recognize the importance of solving problems and work tirelessly to solve any issues that New10 has come across. “What’s really important for me, and why I really respect and love GitLab, is that when we are struggling with something, I see a lot of effort from GitLab to do it better for us. Our software engineers appreciate that,” Kolyaskin said.

          New10 appreciates that GitLab is a technology-oriented company, with an engineering focus, as opposed to a business focus. “When I’m working with GitLab, I just open your road map and then I think, ‘Yeah, this would be a roadmap if I were in charge’ … Really GitLab is supporting us by your way of working.”
    customer_success_stories:
      title: Customer success stories
      link:
        text: See all stories
        href: /customers/
      read_story: Read story
      stories:
        - title: Goldman Sachs
          image: /nuxt-images/customers/goldman_sachs_case_study.png
          description: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
          link: /customers/goldman-sachs/
        - title: Siemens
          image: /nuxt-images/customers/siemens.png
          description: How Siemens created an open source DevOps culture with GitLab
          link: /customers/siemens
        - title: Fanatics
          image: /nuxt-images/customers/fanatics_case_study.png
          description: Fanatics' successful GitLab CI transition empowers innovation cycles and speed
          link: /customers/fanatics/
