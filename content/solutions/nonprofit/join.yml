---
  title: Join GitLab for Nonprofits
  description: The single application to accelerate your nonprofit. We are offering our top tiers for free to nonprofits that meet the eligibility criteria. Learn more!
  template: 'industry'
  next_step_alt: true
  components:
    - name: 'solutions-hero'
      data:
        title: Join GitLab for Nonprofits
        subtitle: Utilize GitLab to scale your nonprofit*
        footnote: " *The GitLab for Nonprofit Program operates on a first come-first served basis each year. Once we reach our donation limit, the application will no longer be available. "
        title_variant: 'heading1-bold'
        mobile_title_variant: 'heading1-bold'
        primary_btn:
          url: /solutions/nonprofit/join/#nonprofit-program-application
          text: Apply Now
          data_ga_name: Apply Nonprofit
          data_ga_location: header
          icon:
            variant: product
            name: arrow-down
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image: gitLab for Nonprofits"
    - name: 'nonprofit-overview'
      data:
        blocks:
          - title: You'll receive
            offers:
              - list:
                - |
                  Free Ultimate license for a year (SaaS or self-managed) up to 20 seats. Additional seats may be requested but may not be granted.*
        footnote: '*GitLab support is not included with the Nonprofit license.'
        information:
          title: Have questions about the application?
          text: 
            - Read our FAQs for more information
          link:
            url: /solutions/nonprofit/join/#frequently-asked-questions
    - name: 'open-source-form-section'
      data:
        title: Nonprofit program application
        blocks:
          - title: "Requirements"
            content: |
              In order to be eligible for the GitLab for Nonprofits Program, each nonprofit must be a Registered 501c3 (or jurisdictional equivalent) Nonprofit Organization in good standing that align with GitLab’s [Values](https://handbook.gitlab.com/handbook/values/). A “Registered Nonprofit Organization” is one that has been registered with the local government or authorized agency within its applicable local, state, provincial, federal or national government. 
          - title: 
            content: |    
              GitLab prioritizes Registered Nonprofit Organizations that help advance GitLab’s social and environmental key topics that were defined in [GitLab’s 2022 materiality assessment](https://about.gitlab.com/handbook/legal/ESG/). GitLab’s current social and environmental key topics are:
          - title: 
            content: |
              * **Diversity, Inclusion and Belonging**
              * **Talent management and engagement**
              * **Climate action and greenhouse gas emissions**
          - title: 
            content: |
              GitLab does not issue licenses to Registered Nonprofit Organizations located in China as that market is provided for by GitLab’s joint venture in the region. 
          - title: 
            content: |
              As [GitLab generally avoids discussing politics or religion in public forums](https://handbook.gitlab.com/handbook/values/#religion-and-politics-at-work) because it is easy to alienate people with a minority opinion, we also generally avoid partnering with political or religious-oriented Registered Nonprofit Organizations. 
          - title: 
            content: |
              Upon acceptance, program members are subject to the [GitLab Subscription Agreement](https://about.gitlab.com/handbook/legal/subscription-agreement/). The decision to issue a GitLab Startup license is always at the sole discretion of GitLab.
          - title: 
            content: |              
              All nonprofits will be vetted through our partner, [TechSoup](https://www.techsoup.org/).
          - title: "Renewal"
            content: |
             The GitLab for Nonprofits licenses must be renewed annually. Program requirements may change from time to time, and we'll need to make sure that returning members continue to meet them.
          - title: 
            content: |    
            Before applying for renewal:
          - title: 
            content: |
              * Check your permissions. The person claiming the renewal for the subscription must be the same person who created the subscription in the GitLab Customer Portal for this institution.

              * If you want a different person to claim the renewal, the existing owner needs to [transfer ownership of the Customers Portal account](https://docs.gitlab.com/ee/subscriptions/#change-account-owner-information). If the existing owner is no longer able to transfer ownership or renew, please [open a support ticket](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293) to change the owner of the subscription.
          - title: 
            content: |
              Whether applying to the program for the first time or renewing a pre-existing membership, applicants complete the same form.
          - title: 
            content: |
              Once the application form is submitted, the applicant will receive instructions to register on TechSoup, GitLab's verification partner. If verification is completed, GitLab will send an email with instructions to obtain the license. Please follow the instructions carefully.
        form:
          form_id: 3964
          form_header: ''
          datalayer: nonprofits
    - name: 'faq'
      data:
        header: Frequently asked questions
        show_all_button: true
        background: false
        normal_header: true
        extra_margin: true
        groups:
        - header: ""
          questions:
            - question: My nonprofit isn’t registered with my local government, but we have a social mission. Do we qualify? 
              answer: >-
                No, nonprofits must be a Registered 501c3 (or jurisdictional equivalent) Nonprofit Organization in good standing to be eligible for the program. 
            - question: Can I request more than 20 seats?
              answer: >-
                You may request additional seats but please note that all requests will not be granted.
            - question: Will the licenses continue to be freee of charge upon renrewal?
              answer: |
                At this time. Program requirements and offerings may change from time to time. We will ensure that all program participants are notified of any changes prior to implementation. 
            - question: How do I get support?
              answer: >-
                Please see the Support for Community Programs docs sections for a detailed description of where to find support.
