---
  title: GitLab for Educationプログラムに参加する
  description: GitLab for Educationにより、お近くの教室でDevOpsを行えます。今すぐ申請してDevOpsを始めましょう！
  components:
    - name: 'solutions-hero'
      data:
        title: 教育向け GitLab
        subtitle: キャンパスへのDevOpsの導入
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: '#application'
          text: GitLab for Educationプログラムに参加する
          data_ga_name: join education program
          data_ga_location: header
          icon:
            name: arrow-down
            variant: product
        image:
          image_url: /nuxt-images/solutions/education.jpeg
          alt: "Image: gitlab for education"
          rounded: true
    - name: tabs-menu
      data:
        column_size: 8
        menus:
          - id_tag: requirements
            text: 要求事項
            data_ga_name: requirements
            data_ga_location: header
          - id_tag: application
            text: アプリケーション
            data_ga_name: application
            data_ga_location: header
          - id_tag: renewal
            text: 更新
            data_ga_name: renewal
            data_ga_location: header
          - id_tag: frequently-asked-questions
            text: よくある質問
            data_ga_name: frequently asked questions
            data_ga_location: header
    - name: copy-media
      data:
        block:
          - header: 要求事項
            aos_animation: fade-up
            aos_duration: 500
            hide_horizontal_rule: true
            metadata:
              id_tag: requirements
            id: requirements
            miscellaneous: |
              ##### GitLab for Education プログラムに参加するには、各教育機関が次の要件を満たしている必要があります。

              * <b>認定:</b>教育機関は、地方、州、州、連邦、または国の認定機関によって認定されている必要があります。[詳細はこちら](/handbook/marketing/community-relations/community-programs/education-program/#gitlab-for-education-program-requirements){data-ga-name="accredited" data-ga-location="body"}。
              * <b>主な目的の教育:</b>教育機関は、登録された学生を教育することを主な目的とする必要があります。
              * <b>学位授与:</b>教育機関は、準学士、学士、および大学院の学位などの学位を積極的に授与します。
              * <b>非営利:</b>教育機関は非営利である必要があります。営利団体は対象外です。

              ##### GitLab for Education ライセンスは、次の目的でのみ使用できます。

              * <b>教育的使用:</b>教育機関の教育機能の一部である学術教育を含む、学生の学習、訓練、または開発に直接関連するアクティビティ、または
              * <b>非営利の学術研究:</b>収益を生み出すために誰もが商業的に使用するための結果、作品、サービス、またはデータを生成しない非営利の研究プロジェクトを実施します。第三者の要求および利益のために行われる研究は、GitLab for Educationライセンスの下で承認されていません。
              * <b>GitLab for Educationライセンスでのインシデントの実行、管理、または操作は許可されていません。</b> GitLabは、キャンパス全体で使用できるアカデミック割引と特別価格を提供しています。[詳細はこちら](/solutions/education/campus/){data-ga-name= "campus pricing" data-ga-location="body"}。

              * <b>注:</b>現時点では、13歳未満の学生を登録する教育機関は、GitLab SaaSの対象外です。これらの機関は、GitLab自己管理ライセンスを取得することができます。

              ##### 申請者は次のことを行う必要があります

              * <b>教員またはスタッフ:</b>教育機関でフルタイムで雇用されている教員またはスタッフのみが適用できます。学生に直接ライセンスを発行することはできません。
              * <b>メールドメイン:</b>申請者は、代表機関のメールドメインで申請する必要があります。個人のメールドメインは受け入れられません。

              ##### 原産国


              * GitLab, Inc.は、中国にある教育機関にライセンスを発行しません。中国での教育用のライセンスの取得についての詳細は、[JiHuにお問い合わせください](mailto: ychen@gitlab.cn)。[JiHuの詳細](/blog/2021/03/18/gitlab-licensed-technology-to-new-independent-chinese-company/){data-ga-name="more about JiHu" data-ga-location="body"}。

              #### GitLab for Education 契約

              * 承認されると、すべてのプログラムメンバーは[GitLab for Educationプログラム契約](/handbook/legal/education-agreement/){data-ga-name="education agreement" data-ga-location="body"}の対象となります。

              #### プログラムの特典

              * トップレベルの機能(SaaSまたは自己管理)のライセンスごとに無制限のシート数。シート数は、来年中にこのライセンスを使用するさまざまなユーザーの数です。
              * シート数とライセンスの種類(SaaSまたは自己管理)は、更新時または要求に応じて変更できます。
              * GitLabのサポートは、教育用のライセンスには含まれていません。
              * サブスクリプションには50,000 CI Runner分間が含まれています。([追加の分数は購入する必要があります](https://docs.gitlab.com/ee/subscriptions/#purchasing-additional-ci-minutes){data-ga-name="additional minutes" data-ga-location="body"})。
    - name: copy-form
      data:
        form:
          external_form:
            url: https://offers.sheerid.com/gitlab/university/teacher/
            width: 800
            height: 1300
        header: アプリケーション
        metadata:
          id_tag: application
        form_header: アプリケーションフォーム
        datalayer: sales
        content: |
          ## アプリケーションプロセス
          * 右側のアプリケーションフォームに記入します。できるだけ正確かつ完全な情報を入力してください。
          
          * GitLabは、信頼できるパートナーであるSheerIDを使用して、資格のある教育機関の現在の教師、教員、またはスタッフであることを確認します。

          ## 何を期待します
          アプリケーションフォームに記入した後、認証された場合は、ライセンスを獲得するための手順が記載された認証メールが届きます。注意して指示に従ってください。

          ## ヘルプとサポート
          カスタマーポータルでライセンスを獲得する際に問題が発生した場合は、[GitLabサポートポータル]( https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293)でサポートチケットを開き、[ライセンスと更新の問題]を選択してください。
    - name: copy
      data:
        aos_animation: fade-up
        aos_duration: 500
        block:
          - header: 更新
            metadata:
              id_tag: renewal
            id: renewal
            subtitle: 更新を申請
            column_size: 10
            text: |
              GitLab for Educationライセンスは毎年更新する必要があります。プログラムの要求事項は随時変更される可能性があります。また、復帰メンバーが引き続き要求事項を満たしていることを確認する必要があります。

              更新を申請する前に:

              * 権限を確認してください。サブスクリプションの更新を申請する方は、この機関のGitLabカスタマーポータルでサブスクリプションを作成した方でなければなりません。
              *別のユーザーが更新を要求する場合は、既存のオーナーが[カスタマーポータルアカウントの所有権を移管]( https://docs.gitlab.com/ee/subscriptions/#change-account-owner-information)する必要があります。既存のオーナーが所有権を譲渡または更新できなくなった場合は、[サポートチケットを開いて](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293)サブスクリプションの所有者を変更してください。


              初めてプログラムに応募する場合でも、既存のメンバーシップを更新する場合でも、応募者は同じフォームに記入します。

              アプリケーションフォームに記入した後、認証された場合は、ライセンスを獲得するための手順が記載された認証メールを受信します。注意深く指示に従ってください。
             
             
    - name: faq
      data:
        metadata:
          id_tag: frequently-asked-questions
        aos_animation: fade-up
        aos_duration: 500
        header: よくある質問
        groups:
          -
            questions:
              - question: 研究はEducationライセンスの資格がありますか？
                answer: |
                  はい。研究は、教育機関が適格であり、研究が非商業的な学術研究である場合に適格です。詳細については、プログラムの要求事項をご覧ください。
              - question: 同じライセンスキーで複数の自己管理インスタンスを実行できますか？
                answer: |
                  はい。同じライセンスキーで複数の自己管理インスタンスを有効化できます。
              - question: GitLabはなぜ学生に無料ライセンスを直接提供しないのですか？
                answer: 当社のGitLab教育プログラムの提供は、個人ではなく機関(エンタープライズレベル)に直接発行されることを意図しています。私たちは、無制限のシート数とトップクラスのライセンスを提供しており、機関のすべての学生がGitLabが提供する最高のサービスにアクセスできるようにしています。個々の学生がGitLabライセンスを希望する可能性があることを理解していますが、現時点では、個人にライセンスを付与するためのロジスティクスがありません。すべての学生に、教員またはスタッフのスポンサーを見つけて、プログラムに応募することをお勧めします。学生はまた、GitLab.comの無料サブスクリプションティアや、無料のセルフマネージドオファリングの無料ダウンロードを確認する必要があります。さらに高度な機能を試すには、30日間の試用版を申請することもできます。
              - question: プロジェクトの表示レベルをどのように管理すればよいですか？
                answer: |
                  GitLabの親グループのメンバーである場合、すべての子に自動的にアクセスできます。GitLabは、親グループよりも厳しいサブグループをサポートしていません。ただし、サブグループの一部であることにより、親グループへのアクセスが許可されるわけではありません。
                  \
                  これを行う最善の方法は、全員をそれぞれのサブグループのメンバーにし、トップレベルのグループには管理者のみを配置することです。
              - question: このライセンスは、ITサービスを実行するためにIT部門で使用できますか？
                answer: |
                  いいえ、ITプロフェッショナルによる使用、または機関自体を運営するための管理上の使用は対象外です。GitLab Educationライセンスは、教育または研究目的でのみ使用できます。ITプロフェッショナル向けにGitLabを使用することに興味がある場合は、当社のセールスチームにお問い合わせください。
              - question: 学生は卒業後にGitLabインスタンスを使用できますか？
                answer: |
                  GitLab Educationライセンスは、教育機関に直接発行されます。したがって、学生は所属の機関からアクセスが提供されなくなった場合、自分のライセンスを購入する必要があります。
              - question: エンドユーザーライセンス契約への変更は許可されていますか？
                answer: |
                  現時点では、ユーザー契約の変更に対応することはできません。ご不明な点がございましたら、education@gitlab.comまでメールでお問い合わせください。
              - question: SSLでLDAPを介してユーザーを認証することは可能ですか？
                answer: |
                  GitLabの自己管理バージョンでのみ可能です。サーバーはDNS名をLDAPサーバーに使用できるため、厳密には、静的IPを必要としません。
              - question: 今後、シート数を増やすことは可能ですか？
                answer: |
                  既存のライセンスのシート数を増やしたい場合は、education@gitlab.comまでメールでお問い合わせください。追加シートのアドオン見積もりを準備します。
              - question: 誰がサブスクリプションにカウントされますか？
                answer: |
                  詳細については、ライセンスとサブスクリプションのFAQをご覧ください。
              - question: どうやってサポートを受けることができますか？
                answer: |
                   サポートの受け方の説明については、[コミュニティプログラムのサポート]( https://docs.gitlab.com/ee/subscriptions/#support-for-community-programs)ドキュメントのセクションをご覧ください。GitLab for Educationライセンスのサポートを別途購入することはできなくなりましたのでご注意ください。代わりに、対象となる機関には、[GitLab for Campusesサブスクリプション](/solutions/education/campus/)を購入するオプションがあります。