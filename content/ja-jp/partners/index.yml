---
  title: クラウド、プラットフォーム、テクノロジー提携パートナー
  description: GitLabパートナーの概要や参加している企業、どのようなサービスを提供しているのか、またパートナーになる方法について学びましょう。
  components:
    - name: 'hero'
      data:
        title: GitLabパートナープログラム
        text: 「早く行きたければ、一人で進め。遠くまで行きたければ、みんなで進め。」（ことわざ）
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: 今すぐ申し込む
          url: https://partners.gitlab.com/English/register_email.aspx
          data_ga_name: apply today
          data_ga_location: header
        secondary_btn:
          text: チャネルパートナーを探す
          url: https://partners.gitlab.com/English/directory/search?f0=Partner+Certifications&f0v0=Professional+Services&l=United+States&Adv=none
          data_ga_name: find a channel partner
          data_ga_location: header
        tertiary_btn:
          text: 提携パートナーを探す
          url: /partners/technology-partners/
          data_ga_name: find an alliance partner
          data_ga_location: header
        image:
          url: /nuxt-images/solutions/infinity-icon-cropped.svg
          alt: "画像: GitLabパートナー"
    - name: 'copy-block'
      data:
        header: 概要
        anchor_id: Overview
        top_margin: slp-mt-md-64
        column_size: 9
        blocks:
          - text: |
              GitLabは、主要パートナーによるグローバルエコシステムとの提携の下、拡大を続けるDevSecOpsとデジタルトランスフォーメーションに関するお客様のニーズをサポートします。オープンコラボレーションへの徹底した取り組みを通じて、ソフトウェアライフサイクル全体にわたって完全なインテグレーション、サポート、サービスを提供するパートナーで構成されるエコシステムを構築しました。

    - name: 'copy-block'
      data:
        header: チャネル、再販、インテグレーション、トレーニングパートナー
        anchor_id: channel-resell-integration-and-training-partners
        column_size: 9
        blocks:
          - text: |
              GitLabのグローバルセールスおよびインテグレーションパートナーは、お客様がデジタルトランスフォーメーションにおいて技術的目標およびビジネス目標を達成できるよう支援します。当社は、大手ソリューションプロバイダー（システムインテグレーター、クラウドプラットフォームパートナー、リセラー、ディストリビューター、マネージドサービスプロバイダー、エコシステムパートナーなど）との提携を通じて、お客様が単一のDevSecOpsプラットフォームとしてGitLabを導入することで得られる価値を最大限に高めます。当社のチャネルパートナーは、ライセンスの再販やエンドユーザーの代理でのライセンス購入に加え、GitLabユーザーに対してデプロイ、インテグレーション、最適化、サービスの管理、トレーニングサービスの提供を行うことが認められています。

              これらのソリューションは、GitLabが最新のソフトウェア駆動型エクスペリエンスをお客様に提供するという使命を果たす上で重要な役割を持ちます。当社はパートナーと協力して、世界中のあらゆる規模と業種の企業や組織が、より効果的に運用するために必要なデジタル変革をリードし、かつ優れたカスタマーエクスペリエンスを提供することを支援します。
    - name: 'partners-showcase'
      data:
        padding: 'slp-my-32 slp-my-md-48'
        items:
          - image:
              src: /nuxt-images/partners/badges/gitlab-select-partner-badge.svg
              alt: パートナーバッジを選択
            title: チャネルリセラーを選択
            text: セレクトパートナーは、GitLabの専門知識に大きな投資を行い、GitLabを中心としたサービスプラクティスを開発し、GitLab製品の経常収益を増やすことが期待されているパートナーです。セレクトパートナートラックへは、GitLabからの招待のみにより参加できます。
          - image:
              src: /nuxt-images/partners/badges/gitlab-open-partner-badge.svg
              alt: オープンパートナーバッジ
            title: オープンチャネルリセラー
            text: 当社のオープントラックは、最低限の要件を満たし、GitLabの販売機会を特定またはサポートするパートナーが利用できます。GitLabオープンパートナーは、取引パートナーであるかどうかにかかわらず、製品の割引や紹介料を得ることができます。リセラー、インテグレーター、その他のセールスおよびサービスパートナーは、オープントラックでプログラムに参加します。
          - image:
              src: /nuxt-images/partners/badges/gitlab-professional-services-partner-badge.svg
              alt: プロフェッショナルサービスパートナーバッジ
            title: グローバルおよびリージョナルシステムインテグレーターおよびプロフェッショナルサービス
            text: 展開、統合、最適化サービスで実装と導入をサポートします。これらのパートナーは、GitLabソフトウェアを再販することもできます。
    - name: 'copy-block'
      data:
        no_decoration: true
        column_size: 9
        blocks:
          - text: |
              GitLabはまた、パートナー認定を提供することで、パートナーがより深くGitLabについての専門知識を開発することを可能にします。GitLabプロフェッショナルサービスパートナー認定により、ユニークなサービスで差別化し、GitLabプラットフォームの導入を促進することができます。さらに、GitLab認定トレーニングパートナーは、GitLabまたはカスタムトレーニングを提供して、カスタマーがGitLabの使用に関する専門知識を向上させることができます。
            link:
              text: チャネルパートナーを探す
              url: https://partners.gitlab.com/English/directory/search?f0=Partner+Certifications&f0v0=Professional+Services&l=United+States&Adv=none
              data_ga_name: find a partner

    - name: 'copy-block'
      data:
        header: クラウド、プラットフォーム、テクノロジー提携パートナー
        anchor_id: cloud-platform-and-technology-alliance-partners
        column_size: 9
        blocks:
          - text: |
              当社は、すべての主要な業界をリードするクラウドおよびテクノロジープロバイダーと協力して、最高にキュレーションされた最新のDevSecOpsプラットフォームを提供しています。当社のパートナーはGitLabと統合して、さまざまな業界やユースケースにおいてカスタマイズされたDevSecOpsソリューションを提供します。これらは、GitLabの使命の重要な要素です。つまり最新のソフトウェア駆動型エクスペリエンスをカスタマーに提供し、イノベーションを育み、変革を刺激する堅牢で繁栄するパートナーエコシステムを通じて「誰もが貢献できる」ことを保証することです。共に、私たちはエンタープライズカスタマが今日の市場で効果的に競争するために必要なデジタル変革をリードすることを可能にします
            link:
              text: 提携パートナーを探す
              url: /partners/technology-partners/
              data_ga_name: find an alliance partner
    - name: 'partners-showcase'
      data:
        padding: 'slp-mt-32 slp-mt-md-48'
        clickable: true
        items:
          - image:
              src: /nuxt-images/partners/badges/gitlab-cloud-partner-badge.svg
              alt: クラウドパートナーバッジ
            text: パブリッククラウドコンピューティングサービスとソフトウェアマーケットプレイスを提供するクラウドサービスプロバイダーとHyper-scalerで、カスタマーはGitLabを調達して展開できます。GitLabのデプロイメントを簡素化しました。主要なクラウドプロバイダーと提携して、より良いソフトウェアをより速く提供します。当社のクラウドネイティブインテグレーションは、開発者が最も信頼する環境へのダイレクトラインです。
            url:
              href: /partners/technology-partners/#cloud-partners
              ga:
                data_ga_name: cloud partner
                da_ga_lcoation: body
          - image:
              src: /nuxt-images/partners/badges/gitlab-platform-partner-badge.svg
              alt: プラットフォームパートナーバッジ
            text: エンタープライズおよびアーキテクチャ全体でGitLabのモジュール性と拡張性を高める最新のクラウド/クラウドネイティブアプリケーションプラットフォームを提供するハードウェアおよびソフトウェアパートナー
            url:
              href: /partners/technology-partners/#platform-partner
              ga:
                data_ga_name: platform partner
                da_ga_lcoation: body
          - image:
              src: /nuxt-images/partners/badges/gitlab-technology-partner-badge.svg
              alt: テクノロジーパートナーバッジ
            text: GitLabとシームレスに統合/相互運用する補完技術を備えた独立系ソフトウェアベンダー(ISV)により、より完全なカスタマーソリューションを実現
            url:
              href: /partners/technology-partners/#technology-partners
              ga:
                data_ga_name: technology partner
                da_ga_lcoation: body
    - name: 'copy-block'
      data:
        header: 成功するパートナーシップの推進
        anchor_id: driving-successful-partnerships
        column_size: 9
        blocks:
          - text: |
              __「早く行きたければ、ひとりで行け。遠くまで行きたければ、みんなで行け」__

              このことわざは、相互の成功に対する私たちのコミットメントを最もよく示しています。GitLabは、一連の強力なパートナーイネーブルメント、トレーニング、および商業プログラムを開発し、パートナーとカスタマーのエコシステムによりDevSecOpsとデジタルトランスフォーメーション投資の完全なメリットを得ることを可能にしました。
            link:
              url: /partners/benefits/
              text: GitLabパートナーのメリットを見る
              data_ga_name: see gitlab partner benefits
    - name: 'quotes-carousel'
      data:
        no_background: true
        quotes:
          - main_img:
              url: /nuxt-images/partners/headshots/HeadshotMichaelMcBride.png
              alt: GitLabの最高収益責任者であるMichael McBrideの顔写真
            quote: |
              「私たちは、10万を超える組織が多くのDevSecOpsツールを統合し、プラットフォームアプローチに移行するのを支援しています。パートナーと協力して、組織のツール、文化、プロセスを変革するサービスを提供することで、GitLabのDevSecOpsプラットフォームによりカスタマーの成功を加速させます」
            author: Michael McBride
            job: GitLabの最高収益責任者
    - name: 'resources'
      data:
        header: パートナーソリューション
        links:
          - name: プロフェッショナルサービス
            link: /services/
            description: |
              実装、展開、統合、最適化サービス
            ga:
              name: プロフェッショナルサービス
              location: body
          - name: マネージド(MSP)サービス
            link: https://partners.gitlab.com/English/directory/search?f0=Services+Offered&f0v0=Managed+%2F+Hosted+Services&Adv=none
            description: |
              ソブリン要件のためのターンキーマネージドGitLabホスティングサービス
            ga:
              name: マネージド(MSP)サービス
              location: body
          - name: トレーニングサービス
            link: https://partners.gitlab.com/English/directory/search?f0=Services+Offered&f0v0=Training&Adv=none
            description: |
              正式な認定とカスタムトレーニングコース
            ga:
              name: トレーニングサービス
              location: body
          - name: パートナー認定
            link: /handbook/resellers/services/
            description: |
              専門的なトレーニングを受けると、GitLabの再販、PS、MSP、トレーニングパートナーになることができます
            ga:
              name: パートナー認定
              location: body
          - name: パートナーポータル
            link: https://partners.gitlab.com/English/
            description: |
              招待者限定のパートナーポータルにアクセスして、ユニークなリソース、ソリューション、取引登録をご覧ください
            ga:
              name: パートナーポータル
              location: body
          - name: レベルアップ
            link: https://partners.gitlab.com/English/?ReturnUrl=/prm/English/c/Training
            description: |
              新しいスキルを学び、職業上の機会を増やしましょう(アクセスにはパートナーポータルのログインが必要です)
            ga:
              name: レベルアップ
              location: body
          - name: パートナートレーニング、認定、およびイネーブルメント
            link: /handbook/resellers/training/
            description: |
              GitLabのスキルと専門知識を伸ばすための役割ベースのトレーニングとイネーブルメント
            ga:
              name: パートナーのトレーニング、認定、およびイネーブルメント
              location: body
          - name: プロフェッショナル認定
            link: /learn/certifications/public/
            description: |
              GitLabユーザーおよびプロフェッショナルエキスパート向けに公開されている認定資格
            ga:
              name: プロフェッショナル認定
              location: body
          - name: テクノロジーの統合とオンボーディング
            link: /partners/technology-partners/integrate/
            description: |
              新しいパートナーは、GitLabと統合/相互運用可能なソリューションを構築できます
            ga:
              name: テクノロジーの統合とオンボーディング
              location: body
    - name: 'copy-block'
      data:
        header: 弊社の推奨パートナーをご紹介します
        anchor_id: meet-our-featured-partners
        column_size: 9
        blocks:
          - text: |
              GitLabは、クラウド、DevSecOps、テクノロジー、ソリューション、再販、トレーニングをリードするパートナーと提携することで、GitLabの利用開始を簡素化し、より良いソフトウェアをより速く提供できるよう支援しています。当社のGitLab対応インテグレーションは、開発者が最も信頼する環境とツールへのダイレクトラインです。
    - name: 'intro'
      data:
        as_cards: true
        logos:
          - name: VMware Tanzu
            image: /nuxt-images/partners/vmware/logo-vmware-tanzu-square.jpg
            url: /partners/technology-partners/vmware-tanzu/
            aria_label: VMware Tanzuパートナー事例へのリンク
          - name: IBM
            image: /nuxt-images/partners/ibm/ibm.png
            url: /partners/technology-partners/ibm/
            aria_label: IBMパートナー事例へのリンク
          - name: Redhat
            image: /nuxt-images/partners/redhat/redhat_logo.svg
            url: /partners/technology-partners/redhat/
            aria_label: Redhatパートナー事例へのリンク
          - name: Hashicorp
            image: /nuxt-images/partners/hashicorp/hashicorp.svg
            url: /partners/technology-partners/hashicorp/
            aria_label: Hashicorpパートナー事例へのリンク
          - name: GCP
            image: /nuxt-images/partners/gcp/GCP.svg
            url: /partners/technology-partners/google-cloud-platform/
            aria_label: GCPパートナーケー事例へのリンク
          - name: AWS
            image: /nuxt-images/partners/aws/aws-logo.svg
            url: /partners/technology-partners/aws/
            aria_label: AWSパートナー事例へのリンク
