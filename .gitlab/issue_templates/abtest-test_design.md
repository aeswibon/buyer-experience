## Business Objective
<!-- A high-level description of the experiment and the hypothesis for positive business impact. --> 

### Experiment Details
<!-- A detailed description outlining the experiment. -->

### Control
<!-- Description of the controlled version. Usually, this would be the existing UI. -->

### Variant
<!-- Description of the variant. -->

### Target and Split 
<!-- Description of the users and how the traffic should be split. All users at a 50/50 split is usually the standard. -->

### Key Performance Indicators 
- KPI 1
- KPI 2

**Metrics to monitor:**
- Metric 1
- Metric 2

### Estimated Experiment Duration
Duration Calculator: https://www.abtasty.com/sample-size-calculator/<br>
<!--Requires a baseline conversion and daily traffic volume.-->

## Engineering Notes
### LaunchDarkly

Please set the conversion metric to measure: <br>      
- Control: <!--on-page event-->
- Variant: <!--on-page event-->

### Google Analytics

Push the following dataLayer code before DOM ready:

```
window.dataLayer = window.dataLayer || [];

dataLayer.push({
 'event': 'launchDarklyExperiment',
 'launchDarklyExperimentName': 'name of experiment',
 'launchDarklyExperimentId': '0 or 1'  
});
```

_Where:_<br> 
- name of experiment  is the dynamic name of the LaunchDarkly experiment
- 0 or 1  0 = control, 1 = variant

Ensure the variant `<a>` element has the following attributes:
`data-ga-name="[name]"` and `data-ga-location="[page section]"`


/label  ~"dex::ab-testing" 
