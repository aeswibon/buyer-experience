# Redirecting `/index` suffixed pages with Cloudflare CDN worker in `www-gitlab-com`

## Background

When using Nuxt, routes are automatically generated based on the folder structure of the `/pages/` directory. The term "index" is reserved by Nuxt for routing, meaning that any URL ending in `/index` will be served from the respective template in the `/pages/` directory. This behavior is inherent to Nuxt's design and its folder structure-based routing system. However, in certain scenarios, developers might want to avoid serving URLs that end in `/index`, opting instead to serve just the base URL.

More context can be found in [this thread.](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/merge_requests/2565#note_1489399972)

## Problem

Due to Nuxt's automatic route generation based on the /pages/ directory structure, the framework will render the route `/pages/topics/*/index` even if it's removed from the `generate` -> `routes()` hook. This leads to a situation where both `/topics/*/` and `/topics/*/index` are served by the `/pages/topics/*/index.vue` or `/pages/topics/_slug.vue` template.

This behavior is not explicitly documented or addressed in Nuxt's official documentation or other resources.

## Solution using redirects

A practical solution is to leverage redirects. These can be implemented using Nuxt middleware or, more effectively, with a Cloudflare CDN worker. The Cloudflare worker can be set up to:

1. Detect URLs ending in `/index` or `/index/` and redirect them to the base URL without the `/index` suffix.
2. Similarly, for URLs ending in `/index.html`, the worker can redirect to the base URL without the `/index.html` suffix.

[Here's a sample code snippet from `www-gitlab-com`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/redirects/cloudflare-wrangler/src/main.ts):

```javascript
  // Redirect paths ending in /index/? to the same path minus this part
  if (pathname.match(/\/index\/?$/)) {
    clientRequestURL.pathname = pathname.replace(/\/index\/?$/, '/');
    return Response.redirect(clientRequestURL.toString(), 301);
  }
```

[More context can be found in this merged MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/127321)

## Limitations

While redirects offer a straightforward solution to the problem, they come with certain limitations:

- Additional redirect steps could introduce a slight delay in page loading for end-users.
- It adds another layer of complexity to the server's configuration, which developers and sysadmins must be aware of and maintain.
- Not addressing the issue at the source (Nuxt's routing system) means that if the underlying behavior of Nuxt changes in future versions, the redirect solution may need adjustments.
