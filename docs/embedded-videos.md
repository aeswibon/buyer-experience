# Embedded video links

Video URLs are different when they are meant to be used as an embedded video in the page. Please try to adjust and adapt to what seems to be working but as a general rule, follow these formats:

## From Youtube

Please try to follow this format when embedding video from YouTube:

``` bash
https://www.youtube.com/embed/< video id from YT >?enablejsapi=1
```

## From Vimeo

Please try to follow this format when embedding video from Vimeo:

``` bash
https://player.vimeo.com/video/< video id from Vimeo >?h=< second video id from Vimeo >&badge=0&autopause=0&player_id=0&app_id=58479
```